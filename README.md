# DEPRECATED - University of Cambridge - Design System

*This repo is now depreacted in favour of: https://gitlab.developers.cam.ac.uk/digital/design-system/helix*

If you are looking to use the Design System then head over to our [NPM package](https://www.npmjs.com/package/@ucam/design-system) for installation and usage instructions.

The repository is organised into a standard mono-repo using [Rush.js](https://rushjs.io/), with each folder being a project.

## Developer Quickstart

Firstly, [install nodeJS](https://nodejs.org/en/) (The LTS version is a good option for compatibility).

1. Install the dependencies:
   ```shell
   npm install -g @microsoft/rush
   rush update
   ```
2. Run a watching build (live updates) of all dependencies of the documentation site (currently, just the design system):
   ```shell
   rush build:watch --to-except @ucam/design-system-docs
   ```
3. In a separate window, run the docs development server:
   ```shell
   cd docs
   rushx develop
   ```

## Common development tasks

Most tasks can be performed via the `rush` commandline:

```shell
# Install & update dependencies
rush update
# Or install without updating
rush install

# Build everything
rush build
# Rebuild everything
rush rebuild

# Test everything
rush lint
rush test

# Create a new changelog entry
rush change

# Add a dependency
cd <project-folder-name>
rush add -p <package-name>

# Run a watching build of the doc's dependencies and a dev server for the docs
rush build:watch --to-except @ucam/design-system-docs
# And (in a different terminal window)
cd docs
rushx develop

# Update *.doc.mdx files with the latest component property table
# (see "Enhancing component documentation.." below)
cd core
rushx generate:docprops
```

## Bootstrapping the CI/CD infrastructure

When running with gitlab ci the deployment requires the following environment variable secrets to be set:

- `NPM_TOKEN` - An access token for the NPM account which will publish
- `TAG_BOT_ACCESS_TOKEN` - A gitlab access token, with repository update access, to create a tag when the deployment has completed

## Code formatting and style

This repo uses a combination of `eslint` for quality checking and `prettier` to enforce style of the code.

This is opinionated and has been chosen because it often leads to code that is easier to pick up by non-team members.

To make it as easy as possible to code without worrying about style, a git pre-commit hook is supplied which automatically formats your code when commiting. This is automatically installed when running `rush update`. It can be bypassed by running `git commit` with the flag `--no-verify`:

```shell
git commit . -m "some fix" --no-verify
```

## Enhancing component documentation with a table of properties

To enhance the component documentation a tool can be run to parse a component's `*Props` interface
and update the component's `*.doc.mdx` with a table documenting the property names, types, etc. The tool
expects two delimiters to be in the markdown file and will update the space between the delimiters
with the latest table:

```
 :
[//]: # "property-table-DO-NOT-EDIT"

..the table will be written here..

[//]: # "property-table-DO-NOT-EDIT"
 :
```

The tool will ignore markdown file if no delimiters have been added. The tool currently ignores
properties that have no description. This is to encourage the developer to add a description to
properties they feel should be documented.
