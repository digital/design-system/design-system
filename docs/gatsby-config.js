/* eslint-env node */
/* eslint-disable @typescript-eslint/no-var-requires */

const intercept = require('intercept-stdout');

module.exports = {
  siteMetadata: {
    title: 'University of Cambridge - Design System',
    description: `description`,
    author: `author`
  },
  flags: {
    PRESERVE_WEBPACK_CACHE: true,
    FAST_DEV: true,
    DEV_SSR: true
  },
  plugins: [
    `gatsby-plugin-pnpm`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/pages`
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `documentation`,
        path: `${__dirname}/../core/src`,
        // Only include *.doc.mdx files
        ignore: [(filename, stats) => stats && stats.isFile() && !filename.endsWith('.doc.mdx')]
      }
    },
    {
      resolve: 'gatsby-plugin-mdx',
      options: {
        defaultLayouts: {
          default: `${__dirname}/src/components/MarkdownLayout.tsx`
        }
      }
    },
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`
      }
    },
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`, // Needed for dynamic images
    `gatsby-plugin-emotion`,
    `gatsby-plugin-top-layout`,
    `gatsby-plugin-material-ui`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `University of Cambridge - Design System`,
        short_name: `Cambridge Design System`,
        start_url: `/`,
        // TODO: replace these with colors from the Design System
        background_color: `#6BBBAE`,
        theme_color: `#6BBBAE`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png` // This path is relative to the root of the site.
      }
    }
  ]
};

// RushJS will error a build if the environment variable CI=1 is set and there are any warnings on stderr.
// We expect some specific warnings so we ignore and redirect them to StdOut. All other warnings are passed to stderr.
// The intercept must be hooked before the gatsby-config is loaded to allow it to catch Plugin warnings.
intercept(
  (txt) => txt,
  (txt) => {
    if (
      // gatsby-plugin-pnpm doesn't officially support gatsby@3
      txt.includes('Plugin gatsby-plugin-pnpm is not compatible with your gatsby version')
    ) {
      console.log(`Warning removed from StdErr: ${txt}`);
      return '';
    } else {
      return txt;
    }
  }
);
