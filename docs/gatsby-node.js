/* eslint-env node */
/* eslint-disable @typescript-eslint/no-var-requires */

const DuplicatePackageCheckerPlugin = require('@cerner/duplicate-package-checker-webpack-plugin');
const path = require('path');

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions;
  // Find all of the documentation files that have matching mdx nodes
  const result = await graphql(`
    query {
      allFile(
        filter: {
          sourceInstanceName: { eq: "documentation" }
          childrenMdx: { elemMatch: { internal: { type: { eq: "Mdx" } } } }
        }
      ) {
        edges {
          node {
            childrenMdx {
              id
              slug
            }
          }
        }
      }
    }
  `);
  if (result.errors) {
    reporter.panicOnBuild('🚨  ERROR: Loading "createPages" query');
  }
  const components = result.data?.allFile?.edges || [];
  components.forEach(({ node }) => {
    node.childrenMdx.forEach((node) => {
      createPage({
        path: `/docs/${node.slug.replace(/\.doc$/, '')}`,
        component: `${__dirname}/src/components/ComponentsDocLayout.tsx`,
        context: { id: node.id }
      });
    });
  });
};

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      alias: {
        // Multiple versions of react-is are included by our dependencies.
        // There don't appear to be any breaking changes between 16.x and 17.x,
        // and the version number is supposed to match the react version, so we point at 17.x version
        'react-is': path.resolve(__dirname, 'node_modules/react-is')
      }
    },
    module: {
      rules: [
        // Some overrides to allow typescript compiler to run in browser without causing gatsby to freak out.
        {
          test: /typescript\.js$/,
          loader: 'string-replace-loader',
          options: {
            multiple: [
              // When compiling pages during gatsby SSR - typescript thinks it's running in node, this causes issues
              // because gatsby hooks the fs module to and complains if you use various methods. To resolve it we force
              // typescript to run as though it is in the browser
              {
                search: /process\.browser/g,
                replace: 'true'
              },
              // Stop webpack complaining that "the request of a dependency is an expression" by removing those requires
              // they are unused by our code anyway
              {
                search: /require\([^"'`]*?\)/g,
                replace: 'undefined'
              }
            ]
          },
          resolve: {
            fallback: {
              // Not used if process.browser = true, but this avoids a warning
              perf_hooks: false
            }
          }
        }
      ]
    },
    plugins: [
      new DuplicatePackageCheckerPlugin({
        // core-js is used with different major versions and shouldn't be collapsed to a single version
        exclude: (instance) => ['core-js'].includes(instance.name),
        // Duplicates of these packages will cause runtime issues, they should error
        alwaysEmitErrorsFor: ['react', '@material-ui/core', '@emotion/react', '@emotion/styled']
      })
    ]
  });
};
