import React, { FC } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import ThemeProvider from '@ucam/design-system/theme/ThemeProvider';
import { light, dark } from '@ucam/design-system/theme';
import { createTheme, responsiveFontSizes, StyledEngineProvider } from '@material-ui/core';

/**
 * Wraps all gatsby pages, providing everything required to run the design system.
 */
const TopLayout: FC = ({ children }) => {
  return (
    <React.Fragment>
      <Helmet>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700,900&display=swap"
        />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
        {/* Only required because we currently include material as a theme */}
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
      </Helmet>
      <StyledEngineProvider injectFirst>
        <ThemeProvider
          themes={{
            Light: light,
            Dark: dark,
            Material: responsiveFontSizes(createTheme())
          }}
        >
          {children}
        </ThemeProvider>
      </StyledEngineProvider>
    </React.Fragment>
  );
};

TopLayout.propTypes = {
  children: PropTypes.node
};

export default TopLayout;
