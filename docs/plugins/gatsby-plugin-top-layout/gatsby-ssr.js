/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import * as React from 'react';
import TopLayout from './TopLayout';

export const wrapRootElement = ({ element }) => {
  return <TopLayout>{element}</TopLayout>;
};
