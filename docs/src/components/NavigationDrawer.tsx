import React, { FC } from 'react';
import { List, ListItem, ListItemText } from '@material-ui/core';
import { graphql, Link, StaticQuery } from 'gatsby';
import { Sidebar } from '@ucam/design-system';

const NavigationDrawer: FC = () => {
  return (
    <Sidebar aria-label="Main navigation">
      <StaticQuery
        query={graphql`
          {
            allSitePage(filter: { path: { regex: "^/docs/" } }, sort: { fields: path }) {
              edges {
                node {
                  path
                }
              }
            }
          }
        `}
        render={(data) => (
          <List>
            <ListItem button component={Link} to="/">
              <ListItemText primary="Home" />
            </ListItem>
            {data.allSitePage.edges.map(({ node }: { node: { path: string } }) => (
              <ListItem button component={Link} key={node.path} to={node.path}>
                <ListItemText primary={node.path.replace(/^\/docs\//, '')} />
              </ListItem>
            ))}
          </List>
        )}
      />
    </Sidebar>
  );
};

export default NavigationDrawer;
