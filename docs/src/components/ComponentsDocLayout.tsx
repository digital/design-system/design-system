/* eslint-disable react/prop-types */
import React, { FC } from 'react';
import { graphql } from 'gatsby';
import { MDXRenderer } from 'gatsby-plugin-mdx';
import MarkdownLayout from './MarkdownLayout';

interface ComponentsDocLayoutProps {
  data: {
    mdx: {
      body: string;
    };
  };
}

const ComponentsDocLayout: FC<ComponentsDocLayoutProps> = ({ data }) => {
  return (
    <MarkdownLayout>
      <MDXRenderer>{data.mdx.body}</MDXRenderer>
    </MarkdownLayout>
  );
};

export default ComponentsDocLayout;

export const pageQuery = graphql`
  query MDXQuery($id: String!) {
    mdx(id: { eq: $id }) {
      id
      body
    }
  }
`;
