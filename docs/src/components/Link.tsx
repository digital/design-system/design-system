import React from 'react';
import MuiLink, { LinkProps } from '@material-ui/core/Link';
import { Link as GatsbyLink, GatsbyLinkProps } from 'gatsby';

const Link = React.forwardRef<HTMLAnchorElement | HTMLSpanElement, LinkProps & GatsbyLinkProps<unknown>>(
  function Link(props, ref) {
    // eslint-disable-next-line react/prop-types
    if (props.to) {
      return <MuiLink component={GatsbyLink} ref={ref} {...props} />;
    } else {
      return <MuiLink ref={ref} {...props} />;
    }
  }
);

export default Link;
