/* eslint-disable react/prop-types */
import React, { FC, useEffect, useLayoutEffect, useReducer, useState } from 'react';
import { LiveEditor, LivePreview, LiveError, LiveProvider } from 'react-live';
import { transpile, JsxEmit, ScriptTarget } from 'typescript';
import * as designSystem from '@ucam/design-system';
import * as materialUICore from '@material-ui/core';
import { Card, CardActions, CardContent, Collapse } from '@material-ui/core';
import { Language, PrismTheme } from 'prism-react-renderer';

interface InteractiveCodeSnippetProps {
  initialCode: string;
  /**
   * The language used for syntax highlighting
   */
  language: Language;
  /**
   * Disables the editor
   */
  renderOnly: boolean;
  /**
   * The theme for the editor
   */
  theme: PrismTheme;
}

interface TransformOptions {
  /**
   * Improves the readability of the generated Javascript by attempting to match the source's whitespace.
   * Note: This may break sourcemaps
   */
  preserveWhitespace?: boolean;
  /**
   * Attempt to generate inline sourcemaps for the provided code
   */
  sourceMap?: boolean;
}

/**
 * Transpiles TypeScript code into JavaScript
 */
const transformCode = (snippet: string, options: TransformOptions = {}) => {
  const defaultedOptions = {
    preserveWhitespace: false,
    sourceMap: true,
    ...options
  };

  const compilerOptions = {
    jsx: JsxEmit.Preserve,
    target: ScriptTarget.ES2015,
    removeComments: false
  };

  if (defaultedOptions.sourceMap) {
    // TODO: Fix SourceMaps - Our import hacking seems to break them... it is also possible that react-live's babel compiler also mangles/ignores them
    Object.assign(compilerOptions, {
      inlineSourceMap: true,
      sourceMap: true,
      inlineSources: true
    });
  }

  // Do a slightly better job at preserving new lines than the typescript compiler by adding marker comments
  if (defaultedOptions.preserveWhitespace) {
    try {
      return transpile(snippet.replace(/\n\s*\n/g, '\n/** NEW_LINE **/\n'), compilerOptions).replace(
        /(^|\n)(\s*)\/\*\* NEW_LINE \*\*\/\s*\n/g,
        () => `\n\n`
      );
    } catch (e) {
      // If transpile fails then fall back to transpiling without markers so that the errors don't contain the markers
    }
  }

  return transpile(snippet, compilerOptions);
};

/**
 * Import statements aren't handled by react-live, this swaps them for global variables
 */
const swapImports = (transpiledCode: string, moduleToGlobal: { [modulePath: string]: string }) => {
  // converts imports. eg:
  //    import Button from "@ucam/design-system/Button"
  // into const definitions. eg:
  //    const Button = designSystem.Button
  // Handles { } imports, eg:
  //    import { H1, H2, H3 } from "@ucam/design-system/Typography"
  // Handles imports with line breaks and random spacing, eg:
  //    import {
  //      H1
  //    } from "@ucam/design-system/Typography"
  // Handles, renamed imports, eg:
  //    import { H1 as Hello } from "@ucam/design-system/Typography"
  const result = transpiledCode.replace(
    // Regex magic - it is highly recommended you copy this line into a tool like debuggex.com
    /(?:^|\n)\s*import[\s\n]+((?:[\w\d]+|\{[\s\n]*[\w\d]+(?:\s+as\s+[\w\d]+)?(?:[\s\n]*,[\s\n]*[\w\d]+(?:\s+as\s+[\w\d]+)?)*[\s\n]*\}))[\s\n]+from[\s\n]+(["'])([@\w\d-]+(?:\/[\w\d-]+)*)\2\s*;?/g,
    (fullLine, importItem, quote, modulePath) => {
      for (const [p, globalVar] of Object.entries(moduleToGlobal)) {
        if (modulePath === p || modulePath.startsWith(`${p}/`)) {
          return `const ${importItem.replace(/\s+as\s+/g, ': ')} = ${globalVar}${modulePath
            .slice(p.length)
            .replace(/\//g, '.')};`;
        }
      }
      // Wasn't able to substitute import...
      return fullLine;
    }
  );

  const remainingImportCount = result.match(/(^|\n|\s)import\s/g)?.length || 0;
  if (remainingImportCount > 0) {
    console.error('Sandbox Error: Only known imports are supported.');
    return '';
  }
  return result;
};

/**
 * An interactive code block, allowing users to play with the design-system in a browser
 */
const InteractiveCodeSnippet: FC<InteractiveCodeSnippetProps> = ({
  initialCode,
  language,
  renderOnly,
  theme
}) => {
  const [code, setCode] = useState(initialCode);

  const [codeLanguage, setCodeLanguage] = useState('ts');

  const [codeShown, setCodeShown] = useState(false);

  return (
    <LiveProvider
      language={language}
      code={code}
      transformCode={(code) => {
        try {
          return swapImports(transformCode(code), {
            '@ucam/design-system': 'designSystem',
            '@material-ui/core': 'materialUICore',
            react: 'React'
          });
        } catch (e) {
          console.error(e);
          return '';
        }
      }}
      scope={{
        designSystem,
        materialUICore,
        react: {
          useState,
          useReducer,
          useEffect,
          useLayoutEffect
        }
      }}
      theme={theme}
      noInline={true}
    >
      <Card>
        <CardContent>
          <LivePreview />
          <LiveError />
        </CardContent>
        {!renderOnly && (
          <>
            <CardActions>
              {codeShown && (
                <>
                  <button
                    onClick={() => {
                      setCode(initialCode);
                      setCodeLanguage('ts');
                    }}
                  >
                    Reset
                  </button>
                  <select
                    value={codeLanguage}
                    onChange={(evt) => {
                      setCodeLanguage(evt.target.value);
                      if (evt.target.value === 'js')
                        setCode(
                          transformCode(code, {
                            preserveWhitespace: true,
                            sourceMap: false
                          })
                        );
                    }}
                  >
                    <option value="ts">TypeScript</option>
                    <option value="js">JavaScript</option>
                  </select>
                </>
              )}
              <button onClick={() => setCodeShown(!codeShown)}>{codeShown ? 'Hide' : 'Show'} Code</button>
            </CardActions>
            <Collapse in={codeShown}>
              <CardContent>
                <LiveEditor onChange={(code: string) => setCode(code)} />
              </CardContent>
            </Collapse>
          </>
        )}
      </Card>
    </LiveProvider>
  );
};

export default InteractiveCodeSnippet;
