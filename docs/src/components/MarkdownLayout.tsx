/* eslint-disable react/prop-types */
import React, { ComponentType, FC } from 'react';
import {
  Checkbox,
  CheckboxProps,
  Divider,
  Input,
  InputProps,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from '@material-ui/core';
import { MDXProvider, MDXProviderComponentsProp } from '@mdx-js/react';
import { H1, H2, H3, H4, H5, H6, P } from '@ucam/design-system/Typography';
import Layout, { LayoutProps } from './Layout';
import { useTheme } from '@ucam/design-system/theme';
import InteractiveCodeSnippet from './InteractiveCodeSnippet';
import Highlight, { defaultProps } from 'prism-react-renderer';
import vsDark from 'prism-react-renderer/themes/vsDark';
import vsLight from 'prism-react-renderer/themes/vsLight';
import Link from './Link';
import { LinkProps } from '@material-ui/core/Link';
import { GatsbyLinkProps } from 'gatsby';

const mdxComponents: MDXProviderComponentsProp & {
  thead?: ComponentType<unknown>;
  tbody?: ComponentType<unknown>;
  input?: ComponentType<InputProps & CheckboxProps>;
  Link?: ComponentType<LinkProps & GatsbyLinkProps<unknown>>;
} = {
  /* eslint-disable react/display-name */
  p: (props) => <P {...props} />,
  h1: (props) => <H1 {...props} />,
  h2: (props) => <H2 {...props} />,
  h3: (props) => <H3 {...props} />,
  h4: (props) => <H4 {...props} />,
  h5: (props) => <H5 {...props} />,
  h6: (props) => <H6 {...props} />,
  thematicBreak: (props) => <Divider {...props} />,
  blockquote: (props) => <blockquote {...props} />,
  ul: (props) => <ul {...props} />,
  ol: (props) => <ol {...props} />,
  li: (props) => <li {...props} />,
  table: ({ children, ...props }) => (
    <Paper>
      <Table {...props} />
      {children}
    </Paper>
  ),
  tr: (props) => <TableRow {...props} />,
  thead: (props) => <TableHead {...props} />,
  tbody: (props) => <TableBody {...props} />,
  td: ({ align, ...props }) => <TableCell {...props} align={align !== null ? align : undefined} />,
  th: ({ align, ...props }) => <TableCell {...props} align={align !== null ? align : undefined} />,
  pre: (props) => {
    // Markdown ``` code blocks usually result in a <pre><code></code</pre> sandwich
    // We'd like to not wrap code blocks in a <pre> because they change the font-size for elements inside them
    // We already handle wrapping code in <pre> in the code handling block below
    if (props.children?.props?.originalType === 'code') {
      return <>{props.children}</>;
    } else {
      return <pre {...props} />;
    }
  },
  code: ({ className, live, render, children }) => {
    const language = className.replace(/language-/, '');

    const { renderedThemeName } = useTheme();
    const theme = renderedThemeName === 'Dark' ? vsDark : vsLight;

    if (live) {
      return (
        <InteractiveCodeSnippet initialCode={children} language={language} renderOnly={false} theme={theme} />
      );
    }

    if (render) {
      return (
        <InteractiveCodeSnippet initialCode={children} language={language} renderOnly={true} theme={theme} />
      );
    }

    return (
      <Highlight {...defaultProps} code={children} language={language} theme={theme}>
        {({ className, style, tokens, getLineProps, getTokenProps }) => (
          <pre className={className} style={{ ...style, padding: '20px' }}>
            {tokens.map((line, i) => (
              <div key={i} {...getLineProps({ line, key: i })}>
                {line.map((token, key) => (
                  <span key={key} {...getTokenProps({ token, key })} />
                ))}
              </div>
            ))}
          </pre>
        )}
      </Highlight>
    );
  },
  em: (props) => <em {...props} />,
  strong: (props) => <strong {...props} />,
  delete: (props) => <del {...props} />,
  inlineCode: (props) => <code {...props} />,
  hr: (props) => <Divider {...props} />,
  a: (props) => <Link {...props} />,
  img: ({ alt, ...props }) => <img alt={alt} {...props} />,
  input: ({ type, ...props }) => (type === 'checkbox' ? <Checkbox {...props} /> : <Input {...props} />),
  Link: (props) => <Link {...props} />
  /* eslint-enable react/display-name */
};

// eslint-disable-next-line react/prop-types
const MarkdownLayout: FC<LayoutProps> = ({ children, pageContext }) => {
  return (
    <Layout pageContext={pageContext}>
      <MDXProvider components={mdxComponents}>{children}</MDXProvider>
    </Layout>
  );
};

export default MarkdownLayout;
