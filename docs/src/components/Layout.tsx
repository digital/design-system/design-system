/* eslint-disable react/prop-types */
import React, { FC } from 'react';
import Seo from './SEO';
import { AppBar, Toolbar, Typography, experimentalStyled as styled, Box } from '@material-ui/core';
import ThemeChanger from '@ucam/design-system/theme/ThemeChanger';
import NavigationDrawer from './NavigationDrawer';

export interface LayoutProps {
  /* eslint-disable @typescript-eslint/no-explicit-any */
  pageContext?: any;
}

const drawerWidth = 240;

const ApplicationHeader = styled(AppBar)(({ theme }) => theme.mixins.toolbar);

const Layout: FC<LayoutProps> = ({ children, pageContext }) => {
  const title: string = pageContext?.frontmatter?.title;
  const keywords: string[] | undefined = pageContext?.frontmatter?.keywords || undefined;

  return (
    <>
      <Seo title={title} keywords={keywords} />
      <Box sx={{ display: 'flex' }}>
        <ApplicationHeader position="fixed" sx={{ width: `calc(100% - ${drawerWidth}px)`, ml: drawerWidth }}>
          <Toolbar>
            <Typography variant="h6" sx={{ flexGrow: 1 }}>
              News
            </Typography>
            <ThemeChanger />
          </Toolbar>
        </ApplicationHeader>
        <NavigationDrawer />
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            backgroundColor: 'background.default',
            p: 3
          }}
        >
          <Toolbar />
          {children}
        </Box>
      </Box>
    </>
  );
};

export default Layout;
