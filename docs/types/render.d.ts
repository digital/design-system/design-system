// A global that is provided to all .doc.mdx live code snippets
declare const render: (jsx: JSX.Element) => void;
