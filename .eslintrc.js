/* eslint-env node */

module.exports = {
  extends: ['eslint:recommended', 'plugin:react/recommended', 'plugin:@typescript-eslint/recommended'],
  parserOptions: {
    ecmaVersion: 2018
  },
  settings: {
    react: {
      version: '17'
    }
  },
  overrides: [
    {
      files: ['**/*.tsx']
    }
  ],
  rules: {
    '@typescript-eslint/no-unused-vars': ['error', { ignoreRestSiblings: true }]
  }
};
