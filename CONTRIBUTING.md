# Contributing

## Building a new component

Our design system is heavily built on [Material-UI](https://material-ui.com/) components at the moment and therefore has `@material-ui/core` as a peer dependency.

When building new components we are faced with two options:

1. Override Material-UI component styles
2. Build our own component from scratch

### Overriding Material-UI components styles

#### When?

Overriding a Material-UI component using our custom design tokens/theme can be used when we're happy with the API, structure and semantics of the Material-UI version of the component. Using `styleOverrides` is the fastest way for us to produce something Cambridge branded with little overhead. This is especially useful when we're building out the first major version of the design sytem, and we have an incomplete set of primitives.

However, this approach does have it's pitfalls. The developer consuming our design system will be importing components from both `@material-ui/core` and `@ucam/design-system` which can become confusing. It also allows complete customisation of our components, which is a bad thing as we want the constraints and opinions we make to be enforced in the design system.

Use this approach if we need to build something fast and our design specification closely matches that of the Material-UI equivalent.

#### How?

The easiest way to override component styles is to use the `styleOverrides` option inside of our Material-UI theme. In the example below we override the colour of any Material-UI `Button` used within our theme to 'Cambridge Blue'.

```jsx
// @ucam/design-system ThemeProvider

const theme = createTheme({
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          background: colours.cambridgeBlue,
        },
        label: {
          color: colours.white,
        },
      },
    },
  },
});

// Application that consumes the design system

import { Button } from "@material-ui/core";
import { ThemeProvider } from "@ucam/design-system";

const MyApp = () => (
  <ThemeProvider>
    <Button>My background colour is Cambridge Blue!</Button>
  </ThemeProvider>
);
```

Read more on the Material-UI docs about [customising components in the theme.](https://material-ui.com/customization/components/)

### Build our own component from scratch

#### When?

Building our own component gives us complete control over it's usage, capabilities and structure. Our long term goal will be to have everything as a custom component, this however will take time as we expect every component we package to be speced and tested, written in TypeScript and include a document that demos the component usage, options and variants.

Use this approach when we have bespoke design specifications or strong technical opinions on it's structure and interface.

#### How?

Just because we build our own component doesn't mean that everything will need to be built from zero. As we have `@material-ui/core` as a peer dependency it makes sense to use the components they give us as building blocks to accelerate the development process.

In the example below we build a basic `TextField` that has a fixed label, but use the Material-UI `Input` component underneath to benefit from its handy layout, accessibility and form validation props.

```tsx
// @ucam/design-system TextField.tsx
import React from "react";
import MaterialInput from "@material-ui/core/Input";

interface TextFieldProps {
  label: string;
  placeholder: string;
}

const TextField = ({ label, placeholder }: TextFieldProps) => (
  <label>
    {label}
    <MaterialInput placeholder={placeholder} />
  </label>
);

export default TextField;
```
