FROM node:lts-alpine
LABEL maintainer="University of Cambridge Information Services <devops@uis.cam.ac.uk>"

# Need these for rushjs
RUN apk add git python libsecret
# Need to install some extra dependencies to allow mozjpeg (a dependency of gatsby-plugin-sharp) to build
RUN apk add autoconf automake libtool make pkgconf nasm gcc musl-dev
# Stop "lscpu: not found" warnings during gatsby build
RUN apk add util-linux

# Gatsby seems to put part of it's telemetry message on stderr so we disable it
ENV GATSBY_TELEMETRY_DISABLED=1

CMD "/bin/sh"
