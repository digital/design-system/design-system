'use strict';
const fs = require('fs');
const path = require('path');

// Use eval to parse the json file because it is actually jsonc.
const rushConfig = eval(`(${fs.readFileSync('../../rush.json').toString()})`);

/**
 * When using the PNPM package manager, you can use pnpmfile.js to workaround
 * dependencies that have mistakes in their package.json file.  (This feature is
 * functionally similar to Yarn's "resolutions".)
 *
 * For details, see the PNPM documentation:
 * https://pnpm.js.org/docs/en/hooks.html
 *
 * IMPORTANT: SINCE THIS FILE CONTAINS EXECUTABLE CODE, MODIFYING IT IS LIKELY TO INVALIDATE
 * ANY CACHED DEPENDENCY ANALYSIS.  After any modification to pnpmfile.js, it's recommended to run
 * "rush update --full" so that PNPM will recalculate all version selections.
 */
module.exports = {
  hooks: {
    readPackage
  }
};

/**
 * This hook is invoked during installation before a package's dependencies
 * are selected.
 * The `packageJson` parameter is the deserialized package.json
 * contents for the package that is about to be installed.
 * The `context` parameter provides a log() function.
 * The return value is the updated object.
 */
function readPackage(packageJson, context) {
  // @emotion/styled has an unnecessary dependency on @emotion/babel-plugin, it looks like it used to be required
  // in previous versions
  if (packageJson.name === '@emotion/styled') {
    delete packageJson.dependencies['@emotion/babel-plugin']
  }

  if (packageJson.name === 'react-redux') {
    packageJson.peerDependencies['redux'] = '^2.0.0 || ^3.0.0 || ^4.0.0'
  }

  if (packageJson.name === 'react-codemirror2' || packageJson.name === 'react-datetime' || packageJson.name === "create-react-context") {
    packageJson.peerDependencies['react'] = '^16.0.0 || ^17.0.0'
  }
  
  // We're not using gatsby's eslint plugin
  if (packageJson.name === 'gatsby') {
    delete packageJson.dependencies['eslint-config-react-app']
  }

  // Gatsby forces express-graphql to use graphql 15
  // https://github.com/gatsbyjs/gatsby/issues/16840
  if (packageJson.name === 'express-graphql') {
    packageJson.peerDependencies['graphql'] = '^14.4.1 || ^15.0.0'
  }

  // gatsby-plugin-material-ui doesn't handle alpha versions of @material-ui/styles
  if (packageJson.name === 'gatsby-plugin-material-ui') {
    packageJson.peerDependencies['@material-ui/styles'] = '>=4.0.0 || ^5.0.0-alpha'
  }

  // react-live uses react-simple-code-editor which expects react@^16.0.0, we're using 17
  // https://github.com/satya164/react-simple-code-editor/issues/75
  if (packageJson.name === 'react-simple-code-editor' || packageJson.name === "@reach/router") {
    packageJson.peerDependencies['react'] = '^16.0.0 || ^17.0.0';
    packageJson.peerDependencies['react-dom'] = '^16.0.0 || ^17.0.0';
  }

  // gatsby-plugin-pnpm hasn't been tested against gatsby@^3.0.0
  // https://github.com/Js-Brecht/gatsby-plugin-pnpm/issues/6
  if (packageJson.name === 'gatsby-plugin-pnpm') {
    packageJson.peerDependencies['gatsby'] = '~2.x.x || ^3.0.0';
  }

  // Rush doesn't handle relative workspace dependencies (eg: workspace:../core/dist) particularly well,
  // it replaces with workspace:version-number (eg: workspace:1.0.0) when running `rush version` or `rush publish -a`.
  // It does handle workspace:*, so we use that and use pnpmfile.js to swap the workspace:* for a relative version.
  // This has to factor in that rush may be using a publishFolder for output.
  // https://github.com/microsoft/rushstack/issues/728#issuecomment-729124957
  [packageJson.dependencies, packageJson.devDependencies, packageJson.peerDependencies]
    .filter(deps => deps !== undefined)
    .forEach(deps => {
      Object.entries(deps)
        .filter(([, version]) => version === 'workspace:*')
        .forEach(([pkg]) => {
          const installingProjectDir = getRushProject(packageJson.name).projectFolder;
          const dependencyBuildDir = getRushProjectPublishDir(pkg);

          const dbdRelativeToDirname = `${__dirname}/../../${dependencyBuildDir}`;

          // If the build publish folder doesn't exist (or is empty) then create a dummy one (with a package.json)
          // to allow the install to continue.
          if (!fs.existsSync(dbdRelativeToDirname) || fs.readdirSync(dbdRelativeToDirname).length === 0) {
            fs.mkdirSync(dbdRelativeToDirname, {recursive: true});
            fs.writeFileSync(
                `${dbdRelativeToDirname}/package.json`,
                `{\n`+
                `  "name": "${pkg}",\n` +
                // Prevent accidental publication
                `  "private": true\n` +
                `}\n`
            );
            console.warn(
              `\x1b[31m${pkg} has not yet been built, a dummy folder has been created. To resolve run:`,
                `"rush build --to ${pkg}", or "rush build".\x1b[0m`
            );
          }

          deps[pkg] = `workspace:${path.relative(installingProjectDir, dependencyBuildDir)}`;
        });
    });

  return packageJson;
}

function getRushProject(packageName) {
  const project = rushConfig.projects.find(project => project.packageName === packageName);
  if (project) {
    return project;
  } else {
    throw Error(`Could not find project with packageName: ${packageName}`)
  }
}

function getRushProjectPublishDir(packageName) {
  const project = getRushProject(packageName);
  if (project.publishFolder !== undefined) {
    return `${project.projectFolder}/${project.publishFolder}`;
  } else {
    return project.projectFolder;
  }
}
