/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import typescript from 'rollup-plugin-typescript2';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import generatePackageJson from 'rollup-plugin-generate-package-json';
import copy from 'rollup-plugin-copy';
import glob from 'glob';
import path from 'path';
import assert from 'assert';

import pkg from './package.json';

// Improves debug experience by prettifying the sourcemap paths, eg:
// ../../src/Button/Button.tsx -> /@ucam.uis.devops/design-system/src/Button/Button.tsx
const transformSourceMap = (relativeSourcePath) =>
  relativeSourcePath.replace(/^(\.\.[\\/])*(?=src[\\/])/, `/${pkg.name}/`);

export default [
  {
    input: glob.sync('src/**/index.ts'),
    output: [
      {
        dir: 'dist',
        format: 'cjs',
        preserveModules: true,
        exports: 'auto',
        sourcemap: 'inline',
        sourcemapPathTransform: transformSourceMap
      },
      {
        dir: 'dist/esm',
        format: 'es',
        preserveModules: true,
        sourcemap: 'inline',
        sourcemapPathTransform: transformSourceMap
      }
    ],
    plugins: [
      peerDepsExternal({
        includeDependencies: true
      }),
      typescript(),
      generatePackageJson({
        outputFolder: 'dist',
        baseContents: (pkg) => ({
          name: pkg.name,
          description: pkg.description,
          version: pkg.version,
          license: pkg.license,
          author: pkg.author,
          homepage: pkg.homepage,
          bugs: pkg.bugs,
          repository: pkg.repository,
          main: pkg.main.replace('dist/', ''),
          module: pkg.module.replace('dist/', ''),
          types: pkg.types.replace('dist/', ''),
          sideEffects: pkg.sideEffects,
          dependencies: {},
          peerDependencies: pkg.peerDependencies,
          keywords: pkg.keywords
        })
      }),
      copy({
        targets: [
          { src: '../LICENSE', dest: 'dist' },
          { src: 'README.md', dest: 'dist' }
        ]
      }),
      {
        // Generates a package.json for every directory (module) to improve tree-shaking in cjs code
        // See: https://github.com/mui-org/material-ui/blob/2d7e13ac297636d096917d9bd67a93d2443967c1/scripts/copy-files.js#L18
        name: 'create-module-pkg',
        async generateBundle(options, bundle) {
          if (options.format !== 'cjs') {
            return;
          }

          const chunksRequiringPkgJson = Object.entries(bundle)
            .filter(([, chunk]) => chunk.isEntry)
            // The top level index.js has a package.json created by generatePackageJson, so ignore it
            .filter(([name]) => name !== 'index.js');

          assert(!chunksRequiringPkgJson.some(([name]) => path.basename(name) !== 'index.js'));

          // Create a package.json file for every index.js chunk
          chunksRequiringPkgJson.forEach(([name]) => {
            const folder = path.dirname(name);
            this.emitFile({
              type: 'asset',
              fileName: path.join(folder, 'package.json'),
              source: JSON.stringify(
                {
                  sideEffects: false,
                  main: './index.js',
                  module: `${path.relative(folder, './esm')}/${name}`,
                  types: './index.d.ts'
                },
                undefined,
                2
              )
            });
          });
        }
      }
    ]
  }
];
