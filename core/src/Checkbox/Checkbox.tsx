import React, { FC } from 'react';
import {
  Checkbox as MuiCheckbox,
  CheckboxProps as MuiCheckboxProps,
  FormControlLabel
} from '@material-ui/core';
import { experimentalStyled as styled } from '@material-ui/core';

const StyledCheckbox = styled(MuiCheckbox)(() => ({
  // We don't currently override styles, however we predict we will after testing
  // Leaving this boilerplate in here to pick up from at that point
}));

const StyledCheckboxLabel = styled(FormControlLabel)(() => ({
  marginLeft: 0
}));

type CheckboxLabel = {
  /**
   * A text label to be display alongside the Checkbox element
   */
  label?: string;
};

type CheckboxProps = CheckboxLabel &
  Omit<
    MuiCheckboxProps,
    | 'classes'
    | 'icon'
    | 'checkedIcon'
    | 'indeterminateIcon'
    | 'slot'
    | 'disableRipple'
    | 'disableTouchRipple'
    | 'disableFocusRipple'
    | 'size'
  >;

/**
 * Wrapper than controls the root component
 * If a label is supplied wrap our Checkbox in the Material ControlLabel
 */
const CheckboxWrapper = React.forwardRef<
  HTMLButtonElement,
  CheckboxLabel & MuiCheckboxProps
  // eslint-disable-next-line react/prop-types
>(function Wrapper({ label, ...props }, ref) {
  return label ? (
    <StyledCheckboxLabel label={label} control={<StyledCheckbox {...props} ref={ref} />} />
  ) : (
    <StyledCheckbox {...props} ref={ref} />
  );
});

const Checkbox: FC<CheckboxProps> = React.forwardRef<HTMLButtonElement, CheckboxProps>(function Checkbox(
  { color = 'primary', ...props },
  ref
) {
  return <CheckboxWrapper color={color} ref={ref} {...props} />;
});

Checkbox.propTypes = {
  ...(Checkbox as FC<CheckboxProps>).propTypes
};

export default Checkbox;
