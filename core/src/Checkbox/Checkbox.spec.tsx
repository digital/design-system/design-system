import * as React from 'react';
import Checkbox from './Checkbox';
import { shallow, mount } from 'enzyme';

describe('Checkbox', () => {
  it('renders correctly and matches snapshot', () => {
    expect(shallow(<Checkbox />)).toMatchSnapshot();
  });

  it('renders label when provided', () => {
    const testLabel = 'Hello world';

    const checkbox = mount(<Checkbox label={testLabel} />);
    const label = checkbox.find('.MuiFormControlLabel-label');

    expect(label.hostNodes().text()).toBe(testLabel);
  });
});
