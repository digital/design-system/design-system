import * as React from 'react';
import Button from './Button';
import { shallow } from 'enzyme';

describe('Button', () => {
  it('renders correctly', () => {
    expect(shallow(<Button />)).toMatchSnapshot();
  });
});
