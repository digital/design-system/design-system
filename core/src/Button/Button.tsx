import React, { FC, forwardRef } from 'react';
import { Button as MuiButton, ButtonProps } from '@material-ui/core';
import { experimentalStyled as styled } from '@material-ui/core';
import { alpha } from '@material-ui/core/styles';

declare module '@material-ui/core/Button' {
  interface ButtonPropsVariantOverrides {
    text: true;
  }
}

const StyledButton = styled(MuiButton)(({ theme }) => ({
  textTransform: 'none',
  padding: '8px 16px',
  fontSize: '16px',
  fontWeight: 600,
  boxShadow: 'none',
  '&.MuiButton-containedPrimary': {
    color: 'white',
    '&.Mui-focusVisible': {
      boxShadow: `0 0 0 0.2rem ${alpha(theme.palette.primary.main, 0.2)}`
    }
  },

  '&.MuiButton-containedSecondary': {
    '&.Mui-focusVisible': {
      boxShadow: `0 0 0 0.2rem ${alpha(theme.palette.secondary.main, 0.2)}`
    }
  },

  '&.MuiButton-outlined': {
    borderWidth: '2px'
  },

  '&.MuiButton-outlinedPrimary': {
    borderColor: theme.palette.primary.main,
    '&.Mui-focusVisible': {
      boxShadow: `0 0 0 0.2rem ${alpha(theme.palette.primary.main, 0.2)}`
    }
  },

  '&.MuiButton-outlinedSecondary': {
    borderColor: theme.palette.secondary.main,
    '&.Mui-focusVisible': {
      boxShadow: `0 0 0 0.2rem ${alpha(theme.palette.secondary.main, 0.2)}`
    }
  },

  '&.MuiButton-textPrimary': {
    '&.Mui-focusVisible': {
      boxShadow: `0 0 0 0.2rem ${alpha(theme.palette.primary.main, 0.2)}`
    }
  },

  '&.MuiButton-textSecondary': {
    '&.Mui-focusVisible': {
      boxShadow: `0 0 0 0.2rem ${alpha(theme.palette.secondary.main, 0.2)}`
    }
  },

  '&.Mui-disabled': {
    borderColor: theme.palette.grey[400]
  },

  '&:hover': {
    boxShadow: 'none'
  }
}));

const Button = forwardRef<HTMLButtonElement, ButtonProps>(function Button(
  { variant = 'contained', ...props },
  ref
) {
  return (
    <StyledButton
      {...props}
      variant={variant}
      disableRipple
      disableFocusRipple
      disableTouchRipple
      ref={ref}
    />
  );
});

Button.propTypes = {
  ...(Button as FC<ButtonProps>).propTypes
};

export default Button;
