import React from 'react';
import { mount } from 'enzyme';
import 'jest-extended';
import useLocalStorage from '../useLocalStorage';
import HydrationChecker from '../HydrationChecker';

describe('useLocalStorage', () => {
  it('returns defaultValue if not hydrated', () => {
    const getItem = jest
      .spyOn(Object.getPrototypeOf(global.localStorage), 'getItem')
      .mockReturnValue('should not get this value');

    const results: (string | undefined)[] = [];
    const Component = () => {
      const [value] = useLocalStorage('test', {
        defaultValue: 'Default Value'
      });
      results.push(value);
      return null;
    };

    const consoleWarn = jest.spyOn(console, 'warn').mockImplementationOnce(() => {
      // do nothing.
    });

    mount(<Component />);

    // Expect a hydration warning
    expect(consoleWarn).toBeCalled();

    expect(getItem).toBeCalledWith('test');

    expect(results).toEqual(['Default Value']);
  });

  it('returns defaultValue followed by actual value after hydration', () => {
    const getItem = jest
      .spyOn(Object.getPrototypeOf(global.localStorage), 'getItem')
      .mockReturnValue('Actual Value');

    const results: (string | undefined)[] = [];
    const Component = () => {
      const [value] = useLocalStorage('test', {
        defaultValue: 'Default Value'
      });
      results.push(value);
      return null;
    };

    mount(
      <HydrationChecker>
        <Component />
      </HydrationChecker>
    );

    expect(getItem).toBeCalledWith('test');

    expect(results).toEqual(['Default Value', 'Actual Value']);
  });
});
