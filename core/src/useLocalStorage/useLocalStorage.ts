import { useState } from 'react';
import useHydrated from '../useHydrated';

function localStorageAvailable() {
  // https://gist.github.com/paulirish/5558557
  try {
    const testString = '__test_localStorage__';
    localStorage.setItem(testString, testString);
    localStorage.removeItem(testString);
    return true;
  } catch (e) {
    return false;
  }
}

export interface LocalStorageOptions {
  /**
   * The default value to be returned during hydration or if localStorage does not contain a value.
   */
  defaultValue?: string | undefined;
}

/**
 * A React Hook to gets an item from local storage.
 * It handles SSR by returning the defaultValue for the initial hydration render, regardless of value in localStorage.
 * It must be used inside a `<HydrationChecker/>` (`<ThemeProvider/>` does this automatically).
 */
const useLocalStorage = (
  key: string,
  options?: LocalStorageOptions
): [string | undefined, (value: string | undefined) => void] => {
  const localStorageValue = localStorageAvailable() ? localStorage.getItem(key) : null;
  const hydrated = useHydrated();
  const [currentValue, setCurrentValue] = useState<undefined | string>(
    localStorageValue == null ? options?.defaultValue : localStorageValue
  );

  const setValue = (value: string | undefined) => {
    if (currentValue === value) {
      return;
    }
    if (localStorageAvailable()) {
      if (value == undefined) {
        localStorage.removeItem(key);
      } else {
        localStorage.setItem(key, value);
      }
    }
    setCurrentValue(value);
  };

  if (hydrated) {
    return [currentValue, setValue];
  } else {
    return [options?.defaultValue, setValue];
  }
};

export default useLocalStorage;
