import * as React from 'react';
import { mount } from 'enzyme';
import 'jest-extended';
import Sidebar from '.';
import { ThemeProvider } from '@material-ui/core';
import { light } from '../theme';

describe('Sidebar', () => {
  it('renders mobile Sidebar at sm screen width', () => {
    const smSize = light.breakpoints.values.sm;
    // mock matchMedia as jsdom doesnt support it
    jest.spyOn(window, 'matchMedia').mockImplementation((query) => ({
      matches:
        query.startsWith('(min-width:') && parseInt(query.replace(/\(min-width:(\d+)px\)/, '$1')) < smSize,
      media: query,
      onchange: null,
      addListener: jest.fn(), // deprecated
      removeListener: jest.fn(), // deprecated
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn()
    }));

    mount(
      <ThemeProvider theme={light}>
        <Sidebar />
      </ThemeProvider>
    );

    // The mobile version is a modal (rendered at the top of the document)
    expect(window.document.querySelectorAll('.UcamSidebar-mobile')).toHaveLength(1);
    expect(window.document.querySelectorAll('.UcamSidebar-desktop')).toHaveLength(0);
  });

  it('renders desktop Sidebar when screen width is > sm', () => {
    jest.spyOn(window, 'matchMedia').mockImplementation((query) => ({
      matches: query.startsWith('(min-width:'),
      media: query,
      onchange: null,
      addListener: jest.fn(), // deprecated
      removeListener: jest.fn(), // deprecated
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn()
    }));

    const wrapper = mount(
      <ThemeProvider theme={light}>
        <Sidebar />
      </ThemeProvider>
    );

    const wrapperDomNode = wrapper.getDOMNode<HTMLDivElement>();

    expect(wrapperDomNode.querySelectorAll('.UcamSidebar-mobile')).toHaveLength(0);
    expect(wrapperDomNode.querySelectorAll('.UcamSidebar-desktop')).toHaveLength(1);
  });
});
