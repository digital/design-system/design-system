import React, { FC, forwardRef, WeakValidationMap } from 'react';
import { Drawer, DrawerProps } from '@material-ui/core';
import { experimentalStyled as styled } from '@material-ui/core';
import PropTypes from 'prop-types';
import { SxProps } from '@material-ui/system';
import { Theme } from '@material-ui/core/styles';

type SidebarBaseProps = JSX.IntrinsicElements['nav'] & SidebarProps;

const DesktopDrawer = styled(Drawer)(({ theme }) => ({
  width: '240px',
  '.MuiDrawer-paper': { width: '240px' },
  [theme.breakpoints.down('sm')]: {
    display: 'none'
  }
}));

/* eslint-disable react/prop-types */
const SidebarBase = React.forwardRef<HTMLElement, SidebarBaseProps>(function SidebarBase(
  { children, drawerProps, sx, ...other },
  ref
) {
  const [mobileOpen, setMobileOpen] = React.useState(false);

  return (
    <nav ref={ref} {...other}>
      <DesktopDrawer className="UcamSidebar-desktop" sx={sx} variant="permanent" open {...drawerProps}>
        {children}
      </DesktopDrawer>
      <Drawer
        className="UcamSidebar-mobile"
        sx={sx}
        variant="temporary"
        open={mobileOpen}
        onClose={() => setMobileOpen(false)}
        ModalProps={{
          keepMounted: true // Better open performance on mobile.
        }}
        {...drawerProps}
      >
        {children}
      </Drawer>
    </nav>
  );
});
/* eslint-enable react/prop-types */

type SidebarProps = {
  /**
   * Props applied to the Drawer element.
   */
  drawerProps?: DrawerProps;
  /**
   * The system prop that allows defining system overrides as well as additional CSS styles.
   */
  sx?: SxProps<Theme>;
} & JSX.IntrinsicElements['nav'];

const Sidebar = forwardRef<HTMLElement, SidebarProps>(function Sidebar({ drawerProps = {}, ...rest }, ref) {
  return <SidebarBase drawerProps={drawerProps} {...rest} ref={ref} />;
});

Sidebar.propTypes = {
  /**
   * Additional className(s) to be applied to the root element
   */
  className: PropTypes.string,
  /**
   * Props applied to the Drawer element.
   */
  drawerProps: PropTypes.shape((Drawer as FC<DrawerProps>).propTypes as WeakValidationMap<DrawerProps>),
  /**
   * The system prop that allows defining system overrides as well as additional CSS styles.
   */
  sx: PropTypes.object
};

export default Sidebar;
