import { generateUtilityClasses, generateUtilityClass } from '@material-ui/unstyled';

export function getSidebarUtilityClasses(slot: string): string {
  return generateUtilityClass('UcamSidebar', slot);
}

export const sidebarClasses = generateUtilityClasses('UcamSidebar', [
  'root',
  'drawer',
  'paper',
  'mobile',
  'desktop'
]);
