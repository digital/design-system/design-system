import { decomposeColor, recomposeColor } from '@material-ui/core';

/**
 * Calculates the rgba value that when overlaid on the "backgroundColor" forms the supplied "color".
 * Note: There are many rgba values that may work, it picks the value with the lowest alpha (most transparent)
 *
 * This function only takes rgb strings eg: `"rgb(255,255,255)"`. Conversion from hex is possible using:
 * hexToRgb from @material-ui/core.
 *
 * Justification: We're often given subtle, non-transparent colours by the UX team for use on borders and highlights.
 * These colours provide adequate contrast against the default background colour, but do not guarantee to contrast
 * sufficiently against other colours. A potential improvement (without changing the default case) is to use
 * semi-transparent colours for the foreground colour. There is still no contrast guarantee, but it does produce nicer
 * results when used with most unexpected background colours.
 *
 * For example:
 * 1. A grey border colour on a white background has an adequate contrast ratio.
 * 2. The same grey border colour on a grey background has a poor contrast ratio.
 * 3. A black (but semi-transparent) border colour on a white background appears grey, an adequate contrast.
 * 4. A black (but semi-transparent) border colour on a grey background appears **dark** grey, an adequate contrast.
 *
 * Both 1. and 3. produce the same border colour, but 4. produces a better result than 2.
 */
const equivalentRGBA = (
  colorRGB: string,
  backgroundColorRGB: string,
  preferredAlpha = 0,
  precision = 0.001
): string => {
  console.assert(
    preferredAlpha >= 0 && preferredAlpha <= 1,
    'preferredAlpha must be a number between 0 and 1'
  );
  console.assert(precision > 0, 'precision must be positive');

  const rgbColor = decomposeColor(colorRGB);
  const rgbBackgroundColor = decomposeColor(backgroundColorRGB);

  if (rgbColor.type !== 'rgb') {
    throw Error('Argument colorRGB must be an RGB color');
  }
  if (rgbBackgroundColor.type !== 'rgb') {
    throw Error('Argument backgroundColorRGB must be an RGB color');
  }
  if (rgbColor.colorSpace !== rgbBackgroundColor.colorSpace) {
    throw Error('Color spaces must match');
  }

  // Binary search for the minimum alpha that results in a valid rgba overlay color
  let currentAlpha = preferredAlpha;
  let delta = (1 - preferredAlpha) / 2;
  let result: [number, number, number, number] = [
    rgbColor.values[0],
    rgbColor.values[1],
    rgbColor.values[2],
    1
  ];
  while (delta > precision / 3 && currentAlpha + delta <= 1) {
    const testAlpha = currentAlpha + delta;
    // For the testAlpha calculate the rgb values for the overlayColor
    const overlayColor: [number, number, number] = [
      (rgbColor.values[0] - rgbBackgroundColor.values[0] * (1 - testAlpha)) / testAlpha,
      (rgbColor.values[1] - rgbBackgroundColor.values[1] * (1 - testAlpha)) / testAlpha,
      (rgbColor.values[2] - rgbBackgroundColor.values[2] * (1 - testAlpha)) / testAlpha
    ];
    // Check that each of the overlayColor's RGB values are in the range 0-255
    const isValid = Math.max(...overlayColor) <= 255 && Math.min(...overlayColor) >= 0;
    if (isValid) {
      // Found a valid color, now check for colors with a lower alpha
      result = [...overlayColor, Math.round(testAlpha / precision) * precision];
      delta /= 2;
    } else {
      // Did not find a valid color, now check for colors with a higher alpha
      currentAlpha += delta;
    }
  }

  return recomposeColor({
    type: 'rgba',
    values: result,
    colorSpace: rgbColor.colorSpace
  });
};

export default equivalentRGBA;
