import 'jest-extended';
import equivalentRGBA from './equivalentRGBA';

describe('equivalentRGBA', () => {
  it('handles white on white', () => {
    expect(equivalentRGBA('rgb(255,255,255)', 'rgb(255,255,255)')).toEqual('rgba(255, 255, 255, 0)');
  });

  it('handles black on black', () => {
    expect(equivalentRGBA('rgb(0,0,0)', 'rgb(0,0,0)')).toEqual('rgba(0, 0, 0, 0)');
  });

  it('handles black on white', () => {
    expect(equivalentRGBA('rgb(0,0,0)', 'rgb(255,255,255)')).toEqual('rgba(0, 0, 0, 1)');
  });

  it('handles white on black', () => {
    expect(equivalentRGBA('rgb(255,255,255)', 'rgb(0,0,0)')).toEqual('rgba(255, 255, 255, 1)');
  });

  it('handles white on grey', () => {
    expect(equivalentRGBA('rgb(255,255,255)', 'rgb(128,128,128)')).toEqual('rgba(255, 255, 255, 1)');
  });

  it('handles grey on white', () => {
    expect(equivalentRGBA('rgb(128,128,128)', 'rgb(255,255,255)')).toEqual(`rgba(0, 0, 0, 0.498)`);
  });

  it('handles grey on black', () => {
    expect(equivalentRGBA('rgb(128,128,128)', 'rgb(0,0,0)')).toEqual(`rgba(254, 254, 254, 0.502)`);
  });

  it('uses the preferred alpha if provided', () => {
    expect(equivalentRGBA('rgb(128,128,128)', 'rgb(255,255,255)', 0.75)).toEqual(`rgba(85, 85, 85, 0.75)`);
  });

  it('ignores the preferred alpha if it does not produce a valid rgba', () => {
    expect(equivalentRGBA('rgb(128,128,128)', 'rgb(255,255,255)', 0.3)).toEqual(`rgba(0, 0, 0, 0.498)`);
  });

  it('handles clipped values', () => {
    expect(equivalentRGBA('rgb(255,60,128)', 'rgb(128,128,128)')).toEqual(`rgba(255, 60, 128, 1)`);
  });

  it('handles nearly clipped values', () => {
    expect(equivalentRGBA('rgb(250,5,128)', 'rgb(128,128,128)')).toEqual(`rgba(254, 0, 128, 0.961)`);
  });
});
