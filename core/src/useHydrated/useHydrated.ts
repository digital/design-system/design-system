import React, { useContext } from 'react';

/** @internal */
export const HydrationCheckerContext = React.createContext<boolean | undefined>(undefined);

export interface UseHydratedOptions {
  /**
   * By default useHydrated checks to see if it is run inside a HydrationChecker and warns if not, this disables that warning
   */
  ignoreWarnings?: boolean;
}

/**
 * A hook to check whether we're hydrated
 * Use inside <HydrationChecker />
 */
export const useHydrated: (options?: UseHydratedOptions) => boolean | undefined = (
  options: UseHydratedOptions = {}
) => {
  const value = useContext(HydrationCheckerContext);
  if (value === undefined && !options?.ignoreWarnings) {
    console.warn('useHydrated always returns undefined when outside of a HydrationChecker');
  }
  return value;
};
