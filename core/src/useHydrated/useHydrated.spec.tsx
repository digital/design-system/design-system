import React from 'react';
import { mount } from 'enzyme';
import 'jest-extended';
import useHydrated from '../useHydrated';

describe('useHydrated', () => {
  it('returns undefined and warns if not inside a HydrationChecker', () => {
    const results: (boolean | undefined)[] = [];
    const Component = () => {
      results.push(useHydrated());
      return null;
    };

    const warn = spyOn(console, 'warn');

    mount(<Component />);

    expect(results).toEqual([undefined]);
    expect(warn).toBeCalled();
  });

  it('does not warn if ignoreWarnings is set', () => {
    const results: (boolean | undefined)[] = [];
    const Component = () => {
      results.push(useHydrated({ ignoreWarnings: true }));
      return null;
    };

    const warn = spyOn(console, 'warn');

    mount(<Component />);

    expect(results).toEqual([undefined]);
    expect(warn).not.toBeCalled();
  });
});
