# TextField

TextField combines an <Link to="/docs/Input/Input">Input</Link> with a label.

```tsx live=true
import TextField from '@ucam/design-system/TextField';

render(
  <form>
    <TextField label="Normal" placeholder="Placeholder" sx={{ m: 1 }} />
    <TextField label="Disabled" placeholder="Disabled Placeholder" disabled sx={{ m: 1 }} />
    <TextField
      label="Password"
      placeholder="Password Placeholder"
      type="password"
      defaultValue="secret!"
      sx={{ m: 1 }}
    />
    <TextField
      label="Numeric"
      placeholder="Placeholder"
      inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
      defaultValue={1337}
      sx={{ m: 1 }}
    />
  </form>
);
```

### Hints & Feedback

Use the optional `hint` and `feedback` props to display text to aid completion of the TextField

```tsx live=true
import TextField from '@ucam/design-system/TextField';

render(
  <form>
    <TextField
      label="Email address"
      placeholder="Placeholder"
      hint="We need your email so we can contact you"
      defaultValue="devops@uis.cam.ac.uk"
      feedback="Email address is available!"
    />
  </form>
);
```

### type="number"

It is not recommended to use type="number", instead consider:

```tsx
<TextField inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }} />
```

See [material-ui's documentation](https://next.material-ui.com/components/text-fields/#type-quot-number-quot) for more details.

### Getting/Setting a value

```tsx live=true
import { useState } from 'react';
import TextField from '@ucam/design-system/TextField';

const App = () => {
  const [value, setValue] = useState('hello');
  return (
    <TextField
      label={`The TextField says: ${value}`}
      placeholder="Placeholder"
      sx={{ m: 1 }}
      value={value}
      onChange={(evt) => setValue(evt.target.value)}
    />
  );
};

render(<App />);
```

[//]: # 'property-table-DO-NOT-EDIT'

### Component Properties

| Prop name        | Type                                                                                                                                                                                                                                              | Default  | Description                                                                                                                                                                                                                                                                      |
| ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| label            | string                                                                                                                                                                                                                                            | Required | Label text displayed to describe the TextField's expected input                                                                                                                                                                                                                  |
| autoComplete     | string                                                                                                                                                                                                                                            |          | This prop helps users to fill forms faster, especially on mobile devices. The name can be confusing, as it's more like an autofill. You can learn more about it [following the specification](https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#autofill). |
| autoFocus        | boolean                                                                                                                                                                                                                                           |          | If `true`, the `input` element is focused during the first mount.                                                                                                                                                                                                                |
| characterWidth   | number                                                                                                                                                                                                                                            |          | Used to specify a width of the input based on the expected number of characters                                                                                                                                                                                                  |
| classes          | Partial<InputBaseClasses\>                                                                                                                                                                                                                        |          | Override or extend the styles applied to the component.                                                                                                                                                                                                                          |
| components       | { Root?: ElementType<any\>; Input?: ElementType<any\>; }                                                                                                                                                                                          | {}       | The components used for each slot inside the InputBase. Either a string to use a HTML element or a component.                                                                                                                                                                    |
| componentsProps  | { root?: { as: ElementType<any\>; styleProps?: Omit<InputBaseProps, "components" \| "componentsProps"\> & { hiddenLabel?: boolean; focused?: boolean; }; }; input?: { ...; }; }                                                                   | {}       | The props used for each slot inside the Input.                                                                                                                                                                                                                                   |
| defaultValue     | unknown                                                                                                                                                                                                                                           |          | The default value. Use when the component is not controlled.                                                                                                                                                                                                                     |
| disabled         | boolean                                                                                                                                                                                                                                           |          | If `true`, the component is disabled. The prop defaults to the value (`false`) inherited from the parent FormControl component.                                                                                                                                                  |
| endAdornment     | ReactNode                                                                                                                                                                                                                                         |          | End `InputAdornment` for this component.                                                                                                                                                                                                                                         |
| error            | boolean                                                                                                                                                                                                                                           |          | If `true`, the `input` will indicate an error. The prop defaults to the value (`false`) inherited from the parent FormControl component.                                                                                                                                         |
| feedback         | string                                                                                                                                                                                                                                            |          | Feedback text displayed to communicate state changes                                                                                                                                                                                                                             |
| formControlProps | { children?: ReactNode; classes?: Partial<FormControlClasses\>; color?: "error" \| "primary" \| "secondary" \| "info" \| "success" \| "warning"; ... 9 more ...; variant?: "standard" \| ... 1 more ... \| "filled"; } & CommonProps & Omit<...\> |          | Props to override on the FormControl component                                                                                                                                                                                                                                   |
| fullWidth        | boolean                                                                                                                                                                                                                                           | false    | If `true`, the `input` will take up the full width of its container.                                                                                                                                                                                                             |
| hint             | string                                                                                                                                                                                                                                            |          | Hint text displayed to assist user in completeing the TextField                                                                                                                                                                                                                  |
| id               | string                                                                                                                                                                                                                                            |          | The id of the `input` element.                                                                                                                                                                                                                                                   |
| inputComponent   | ElementType<InputBaseComponentProps\>                                                                                                                                                                                                             | 'input'  | The component used for the `input` element. Either a string to use a HTML element or a component.                                                                                                                                                                                |
| inputMode        | "none" \| "search" \| "text" \| "tel" \| "url" \| "email" \| "numeric" \| "decimal"                                                                                                                                                               |          | Hints at the type of data that might be entered by the user while editing the element or its contents @see https://html.spec.whatwg.org/multipage/interaction.html#input-modalities:-the-inputmode-attribute                                                                     |
| inputProps       | InputBaseComponentProps                                                                                                                                                                                                                           | {}       | [Attributes](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#Attributes) applied to the `input` element.                                                                                                                                                         |
| inputRef         | Ref<any\>                                                                                                                                                                                                                                         |          | Pass a ref to the `input` element.                                                                                                                                                                                                                                               |
| is               | string                                                                                                                                                                                                                                            |          | Specify that a standard HTML element should behave like a defined custom built-in element @see https://html.spec.whatwg.org/multipage/custom-elements.html#attr-is                                                                                                               |
| margin           | "none" \| "dense"                                                                                                                                                                                                                                 |          | If `dense`, will adjust vertical spacing. This is normally obtained via context from FormControl. The prop defaults to the value (`'none'`) inherited from the parent FormControl component.                                                                                     |
| maxRows          | string \| number                                                                                                                                                                                                                                  |          | Maximum number of rows to display when multiline option is set to true.                                                                                                                                                                                                          |
| minRows          | string \| number                                                                                                                                                                                                                                  |          | Minimum number of rows to display when multiline option is set to true.                                                                                                                                                                                                          |
| multiline        | boolean                                                                                                                                                                                                                                           | false    | If `true`, a `textarea` element is rendered.                                                                                                                                                                                                                                     |
| name             | string                                                                                                                                                                                                                                            |          | Name attribute of the `input` element.                                                                                                                                                                                                                                           |
| onBlur           | FocusEventHandler<HTMLTextAreaElement \| HTMLInputElement\>                                                                                                                                                                                       |          | Callback fired when the `input` is blurred. Notice that the first argument (event) might be undefined.                                                                                                                                                                           |
| onChange         | ChangeEventHandler<HTMLTextAreaElement \| HTMLInputElement\>                                                                                                                                                                                      |          | Callback fired when the value is changed. @param event The event source of the callback. You can pull out the new value by accessing `event.target.value` (string).                                                                                                              |
| placeholder      | string                                                                                                                                                                                                                                            |          | The short hint displayed in the `input` before the user enters a value.                                                                                                                                                                                                          |
| readOnly         | boolean                                                                                                                                                                                                                                           |          | It prevents the user from changing the value of the field (not from interacting with the field).                                                                                                                                                                                 |
| required         | boolean                                                                                                                                                                                                                                           |          | If `true`, the `input` element is required. The prop defaults to the value (`false`) inherited from the parent FormControl component.                                                                                                                                            |
| rows             | string \| number                                                                                                                                                                                                                                  |          | Number of rows to display when multiline option is set to true.                                                                                                                                                                                                                  |
| size             | "small" \| "medium"                                                                                                                                                                                                                               |          | The size of the component.                                                                                                                                                                                                                                                       |
| startAdornment   | ReactNode                                                                                                                                                                                                                                         |          | Start `InputAdornment` for this component.                                                                                                                                                                                                                                       |
| sx               | SxProps<Theme\>                                                                                                                                                                                                                                   |          | The system prop that allows defining system overrides as well as additional CSS styles.                                                                                                                                                                                          |
| type             | string                                                                                                                                                                                                                                            | 'text'   | Type of the `input` element. It should be [a valid HTML5 input type](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#Form_%3Cinput%3E_types).                                                                                                                    |
| value            | unknown                                                                                                                                                                                                                                           |          | The value of the `input` element, required for a controlled component.                                                                                                                                                                                                           |

[//]: # 'property-table-DO-NOT-EDIT'
