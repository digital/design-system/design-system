import { generateUtilityClasses, generateUtilityClass } from '@material-ui/unstyled';

export function getTextFieldUtilityClasses(slot: string): string {
  return generateUtilityClass('UcamTextField', slot);
}

export const textFieldClasses = generateUtilityClasses('UcamTextField', [
  'root',
  'labelPlacementStart',
  'labelPlacementTop',
  'labelPlacementBottom',
  'disabled',
  'label',
  'input'
]);
