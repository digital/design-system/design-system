import React, { FC } from 'react';
import { experimentalStyled as styled } from '@material-ui/core';
import FormControl, { FormControlProps } from '@material-ui/core/FormControl';
import FormHelperText, { FormHelperTextProps } from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '../Input';
import { CustomInputProps } from '../Input/Input';
import PropTypes from 'prop-types';
import T, { TypographyProps } from '@material-ui/core/Typography';
import { useId } from '@reach/auto-id';
import clsx from 'clsx';

const LabelContainer = styled('div')(({ theme }) => ({
  marginBottom: theme.spacing(1)
}));

const LabelText = styled(InputLabel)(() => ({
  position: 'static',
  transform: 'none',
  color: 'inherit !important',
  fontWeight: 600,
  textAlign: 'left'
}));

const HintText: FC<TypographyProps> = (props) => <T variant="body2" sx={{ mt: 0.5 }} {...props} />;
const FeedbackText: FC<FormHelperTextProps> = (props) => <FormHelperText sx={{ mt: 1 }} {...props} />;

export type TextFieldProps = CustomInputProps & {
  /**
   * Label text displayed to describe the TextField's expected input
   */
  label: string;
  /**
   * Hint text displayed to assist user in completeing the TextField
   */
  hint?: string;
  /**
   * Feedback text displayed to communicate state changes
   */
  feedback?: string;
  /**
   * Props to override on the FormControl component
   */
  formControlProps?: FormControlProps;
};

const TextField: FC<TextFieldProps> = React.forwardRef(function TextField(props, ref) {
  const { label, hint, feedback, id, type = 'text', disabled, sx, formControlProps, ...rest } = props;

  const inputId = 'UCamInput' + useId(id);
  const hintTextId = 'UCamHint' + useId();
  const feedbackTextId = 'UCamFeedback' + useId();
  const labelId = 'UCamLabel' + useId();

  return (
    <FormControl disabled={disabled} sx={sx} {...formControlProps}>
      <LabelContainer>
        <LabelText shrink={true} htmlFor={inputId} id={labelId}>
          {label}
        </LabelText>
        {hint && <HintText id={hintTextId}>{hint}</HintText>}
      </LabelContainer>
      <Input
        id={inputId}
        type={type}
        inputProps={{
          'aria-labelledby': clsx(label && labelId, hint && hintTextId),
          'aria-describedby': feedback && feedbackTextId
        }}
        {...rest}
        ref={ref}
      />
      {feedback && (
        <FeedbackText id={feedbackTextId} sx={{ ml: 0 }}>
          {feedback}
        </FeedbackText>
      )}
    </FormControl>
  );
});

TextField.propTypes = {
  label: PropTypes.string.isRequired,
  hint: PropTypes.string,
  feedback: PropTypes.string,
  ...Input.propTypes
};

export default TextField;
