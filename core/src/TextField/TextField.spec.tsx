import * as React from 'react';
import TextField from './TextField';
import { mount } from 'enzyme';
import Input from '../Input';
import 'jest-extended';

describe('TextField', () => {
  it('passes defaultValue to Input', () => {
    const testValue = 'value';

    const wrapper = mount(<TextField id="test" label="hello" defaultValue={testValue} />);
    const input = wrapper.find(Input);

    expect(input.prop('defaultValue')).toEqual(testValue);
  });

  it('passes disabled to Input', () => {
    const wrapper = mount(<TextField id="test" label="hello" disabled />);
    const input = wrapper.find(Input);

    expect(input.find('input').props().disabled).toBeTrue();
  });

  it('supports props being passed to the FormControl component', () => {
    const wrapper = mount(<TextField id="test" label="hello" formControlProps={{ fullWidth: true }} />);

    expect(wrapper.find('.MuiFormControl-root').first().hasClass('MuiFormControl-fullWidth')).toBeTrue();
  });
});
