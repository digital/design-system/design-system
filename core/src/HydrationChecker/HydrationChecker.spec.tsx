import React from 'react';
import { mount } from 'enzyme';
import 'jest-extended';
import useHydrated from '../useHydrated';
import { HydrationChecker } from '../index';

describe('HydrationChecker', () => {
  it('renders twice', () => {
    const results: (boolean | undefined)[] = [];
    const Child = () => {
      results.push(useHydrated());
      return null;
    };

    mount(
      <HydrationChecker>
        <Child />
      </HydrationChecker>
    );

    expect(results).toEqual([false, true]);
  });

  it('only returns true when parent is hydrated', () => {
    const results: { level: string; value: boolean | undefined }[] = [];

    const Child = ({ level }: { level: string }) => {
      results.push({ level, value: useHydrated() });
      return null;
    };

    mount(
      <HydrationChecker>
        <Child level="1" />
        <HydrationChecker>
          <Child level="2" />
          <HydrationChecker>
            <Child level="3" />
          </HydrationChecker>
        </HydrationChecker>
      </HydrationChecker>
    );

    expect(results).toEqual([
      { level: '1', value: false },
      { level: '2', value: false },
      { level: '3', value: false },
      { level: '1', value: true },
      { level: '2', value: true },
      { level: '3', value: true }
    ]);
  });
});
