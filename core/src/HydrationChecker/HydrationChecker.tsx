import React, { FC, useState } from 'react';
import useHydrated from '../useHydrated';
import { HydrationCheckerContext } from '../useHydrated/useHydrated';
import { unstable_useEnhancedEffect } from '@material-ui/core';
import PropTypes from 'prop-types';

/**
 * A Component that tells children when the react hydration run has finished.
 * This is required so that the hydration run matches the SSR.
 * Children can retrieve the hydration status using the useHydrated hook.
 * It is generally not required to use this component directly as it is included by `<ThemeProvider />`
 */
const HydrationChecker: FC = ({ children }) => {
  // undefined if this is the top level
  const parentHydrated = useHydrated({ ignoreWarnings: true });

  const [isSomewhatHydrated, setIsSomewhatHydrated] = useState(false);
  unstable_useEnhancedEffect(() => {
    setIsSomewhatHydrated(true);
  }, []);

  // hydration has finished when the top level is hydrated
  const isHydrated = parentHydrated === undefined ? isSomewhatHydrated : parentHydrated;

  return <HydrationCheckerContext.Provider value={isHydrated}>{children}</HydrationCheckerContext.Provider>;
};

HydrationChecker.propTypes = {
  children: PropTypes.node
};

export default HydrationChecker;
