import React from 'react';
import { dark, light, ThemeChanger, ThemeProvider } from '..';
import { createTheme, MenuItem, Select } from '@material-ui/core';
import { mount } from 'enzyme';

const testTheme = createTheme({
  typography: {
    fontFamily: 'TestFontFamily'
  }
});

describe('ThemeChanger', () => {
  it('contains a list of all available themes', () => {
    const themes = {
      Light: light,
      Dark: dark,
      Test: createTheme()
    };

    const wrapper = mount(
      <ThemeProvider themes={themes}>
        <ThemeChanger />
      </ThemeProvider>
    );

    const themeChanger = wrapper.find(ThemeChanger);
    const select = themeChanger.find(Select);
    select.find('div[role="button"]').simulate('mousedown', { button: 0 });
    const openSelect = select.update();
    const menuItems = openSelect.find(MenuItem);
    expect(menuItems.map((menuItem) => menuItem.getDOMNode().textContent)).toEqual([
      'Browser Preference',
      'Light',
      'Dark',
      'Test'
    ]);
  });

  it('changes theme when an item is selected', () => {
    const themes = {
      Light: light,
      Dark: dark,
      Test: testTheme
    };

    const wrapper = mount(
      <ThemeProvider themes={themes}>
        <ThemeChanger />
      </ThemeProvider>
    );

    const themeChanger = wrapper.find(ThemeChanger);
    const select = themeChanger.find(Select);
    select.find('div[role="button"]').simulate('mousedown', { button: 0 });
    const openSelect = select.update();
    const menuItems = openSelect.find(MenuItem);

    const menuItem = menuItems.filterWhere((menuItem) => menuItem.getDOMNode().textContent === 'Test');

    expect(menuItem.length).toBe(1);

    expect(window.getComputedStyle(document.body).getPropertyValue('font-family')).toEqual(
      light.typography.fontFamily
    );

    menuItem.simulate('click');

    expect(window.getComputedStyle(document.body).getPropertyValue('font-family')).toEqual(
      testTheme.typography.fontFamily
    );
  });

  it('selects a theme if initialValue is set', () => {
    const themes = {
      Light: light,
      Dark: dark,
      Test: testTheme
    };

    mount(
      <ThemeProvider themes={themes}>
        <ThemeChanger initialValue="Test" />
      </ThemeProvider>
    );

    expect(window.getComputedStyle(document.body).getPropertyValue('font-family')).toEqual(
      testTheme.typography.fontFamily
    );
  });
});
