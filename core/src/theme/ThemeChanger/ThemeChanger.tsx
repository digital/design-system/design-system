import React, { FC, useState } from 'react';
import {
  FormControl,
  MenuItem,
  NoSsr,
  Select,
  unstable_useEnhancedEffect,
  SelectChangeEvent
} from '@material-ui/core';
import useTheme from '../useTheme';
import useThemeRegister from '../useThemeRegister';
import PropTypes from 'prop-types';

interface ThemeChangerProps {
  /**
   * If set, then the ThemeChanger will set a theme the first time it runs
   */
  initialValue?: string;
}

/**
 * A dropdown picker to select a theme.
 * Must be used inside a `<ThemeProvider />`.
 */
const ThemeChanger: FC<ThemeChangerProps> = (props) => {
  // The user might be deliberately trying to set the initial value to undefined so need to use hasOwnProperty
  const hasInitialValue = Object.prototype.hasOwnProperty.call(props, 'initialValue');
  const [needsInitialValue, setNeedsInitialValue] = useState(hasInitialValue);

  const { requestedThemeName, setTheme } = useTheme();
  const [themes] = useThemeRegister();

  const handleChange = (event: SelectChangeEvent<{ value: unknown }>) => {
    const selectedTheme = (event.target.value || undefined) as string | undefined;
    setTheme(selectedTheme);
  };

  unstable_useEnhancedEffect(() => {
    // We'll only run this on the first pass
    if (needsInitialValue) {
      setNeedsInitialValue(false);
      setTheme(props.initialValue);
    }
  }, [needsInitialValue, props.initialValue]);

  const themeName =
    requestedThemeName !== undefined && themes.has(requestedThemeName) ? requestedThemeName : '';

  return (
    <NoSsr>
      <FormControl>
        <Select
          value={{ value: themeName }}
          onChange={handleChange}
          displayEmpty
          aria-label="Theme"
          name="theme"
          defaultValue={{ value: '' }}
        >
          <MenuItem value="">Browser Preference</MenuItem>
          {Array.from(themes.keys()).map((theme) => (
            <MenuItem value={theme} key={theme}>
              {theme}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </NoSsr>
  );
};

ThemeChanger.propTypes = {
  initialValue: PropTypes.string
};

export default ThemeChanger;
