import React, { useEffect, useState } from 'react';
import { mount } from 'enzyme';
import ThemeProvider from '../ThemeProvider';
import { dark, light } from '..';
import { createTheme } from '@material-ui/core';
import useTheme, { ThemeUpdate } from '../useTheme';
import 'jest-extended';
import { useThemeRegister } from '../index';
import useHydrated from '../../useHydrated';

const testTheme = createTheme({
  typography: {
    fontFamily: 'TestFontFamily'
  }
});

describe('useThemeRegister', () => {
  it('allows themes to be added', () => {
    let renderedTheme: ThemeUpdate | undefined = undefined;

    const Child = () => {
      const isHydrated = useHydrated();
      const [done, setDone] = useState(false);
      const [themes, setThemes] = useThemeRegister();
      renderedTheme = useTheme();
      const { setTheme } = renderedTheme;

      useEffect(() => {
        if (isHydrated) {
          if (!done) {
            setDone(true);
            expect(themes.has('Test')).toBeFalse();
            setThemes({
              Light: light,
              Dark: dark,
              Test: testTheme
            });
          }
        }
      });

      useEffect(() => {
        if (isHydrated && done) {
          setTheme('Test');
        }
      });

      return null;
    };

    mount(
      <ThemeProvider>
        <Child />
      </ThemeProvider>
    );

    expect(window.getComputedStyle(document.body).getPropertyValue('font-family')).toEqual(
      testTheme.typography.fontFamily
    );

    expect(renderedTheme).toEqual({
      requestedThemeName: 'Test',
      renderedThemeName: 'Test',
      theme: testTheme,
      setTheme: expect.any(Function)
    });
  });
});
