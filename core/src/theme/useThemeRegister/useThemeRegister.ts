import React, { useContext } from 'react';
import { Theme } from '@material-ui/core/styles';

export type ThemeRegister = [
  /**
   * The currently registered themes
   */
  Map<string, Theme>,
  /**
   * Update the currently registered themes.
   */
  (themes: { [name: string]: Theme }) => void
];

/** @internal */
export const ThemeRegisterContext = React.createContext<ThemeRegister>([
  new Map(),
  () => {
    throw Error('Must be used inside a ThemeProvider');
  }
]);

/**
 * A hook to get and update the registered themes.
 * Must be used inside a `<ThemeProvider />`
 */
export const useThemeRegister: () => ThemeRegister = () => useContext(ThemeRegisterContext);
