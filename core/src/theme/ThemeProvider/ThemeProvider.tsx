import React, { useCallback, useMemo, useState } from 'react';
import {
  InternalStandardProps as StandardProps,
  CssBaseline,
  ScopedCssBaseline,
  useMediaQuery,
  Theme,
  ThemeProvider as MuiThemeProvider
} from '@material-ui/core';
import { dark, light } from '../themes';
import { ThemeUpdate, ThemeUpdateContext } from '../useTheme/useTheme';
import { ThemeRegisterContext } from '../useThemeRegister/useThemeRegister';
import useLocalStorage from '../../useLocalStorage';
import HydrationChecker from '../../HydrationChecker';
import PropTypes, { Requireable } from 'prop-types';

export interface ThemeProviderProps extends StandardProps<React.HTMLAttributes<HTMLDivElement>> {
  /**
   * A collection of themes and their names
   * default: {
   *   "Light": light,
   *   "Dark": dark
   * }
   */
  themes?: {
    [themeName: string]: Theme;
  };
  /**
   * The name of the default (light) theme
   * default: "Light"
   */
  defaultThemeName?: string;
  /**
   * The name of the dark theme
   * default: "Dark"
   */
  darkThemeName?: string;
  /**
   * Store the current theme in local storage, to save it between browser reloads
   * Note: Uses the same value for the lifetime of the component (cannot be updated)
   * default: false
   */
  disableLocalStorage?: boolean;
  /**
   * Global scoped ThemeProviders apply global styles to the body element
   * Local scoped ThemeProviders apply global styles to a wrapper div
   * Unscoped ThemeProviders do not apply any global styles
   * default: "global"
   */
  scope?: 'global' | 'local' | false;
}

/**
 * A component that provides the app with theming support
 */
const ThemeProvider = React.forwardRef<HTMLDivElement, ThemeProviderProps>(function ThemeProvider(
  props,
  ref
) {
  const {
    children,
    themes = {
      Light: light,
      Dark: dark
    },
    defaultThemeName = 'Light',
    darkThemeName = 'Dark',
    scope = 'global',
    disableLocalStorage: propDisableLocalStorage,
    ...other
  } = props;

  // Note: false during rehydration run, regardless of browser setting...
  // TODO: if we switch to the useHydrated hook and use the standard browser media queries we can possibly avoid extra renders here
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

  // Cannot update the disableLocalStorage value otherwise different hooks would be called on the next line
  // React requires that the same hooks be called in the same order
  const [localStorageEnabled] = useState(!propDisableLocalStorage);
  const [chosenTheme, setChosenTheme] = (localStorageEnabled
    ? () => useLocalStorage('theme')
    : () => useState<string | undefined>(defaultThemeName))();

  const [registeredThemes, setRegisteredThemes] = useState(new Map(Object.entries(themes)));

  let themeName: string;
  let theme: Theme;
  if (chosenTheme !== undefined && registeredThemes.has(chosenTheme)) {
    themeName = chosenTheme;
    theme = registeredThemes.get(themeName) as Theme;
  } else {
    themeName = prefersDarkMode ? darkThemeName : defaultThemeName;
    theme = registeredThemes.get(themeName) || light;
  }

  const setTheme = useCallback(
    (themeName: string | undefined) => {
      console.assert(
        themeName === undefined || registeredThemes.has(themeName),
        `Unrecognised theme: ${themeName}`
      );
      setChosenTheme(themeName);
    },
    [setChosenTheme, registeredThemes]
  );

  const selectedTheme = useMemo<ThemeUpdate>(
    () => ({
      requestedThemeName: chosenTheme,
      renderedThemeName: themeName,
      theme,
      setTheme
    }),
    [chosenTheme, themeName, setTheme]
  );

  const setThemes = useCallback(
    (themes: { [name: string]: Theme }) => {
      setRegisteredThemes(new Map(Object.entries(themes)));
    },
    [setRegisteredThemes]
  );

  const themeRegister: [Map<string, Theme>, (themes: { [name: string]: Theme }) => void] = useMemo(
    () => [registeredThemes, setThemes],
    [registeredThemes, setThemes]
  );

  const cssBaselineWrapper = (() => {
    switch (scope) {
      case 'global':
        return <CssBaseline {...other}>{children}</CssBaseline>;
      case 'local':
        return (
          <ScopedCssBaseline {...other} ref={ref}>
            {children}
          </ScopedCssBaseline>
        );
      default:
        return <>{children}</>;
    }
  })();

  return (
    <ThemeRegisterContext.Provider value={themeRegister}>
      <ThemeUpdateContext.Provider value={selectedTheme}>
        <MuiThemeProvider theme={theme}>{cssBaselineWrapper}</MuiThemeProvider>
      </ThemeUpdateContext.Provider>
    </ThemeRegisterContext.Provider>
  );
});

ThemeProvider.propTypes = {
  children: PropTypes.node,
  themes: PropTypes.objectOf((PropTypes.object as Requireable<Theme>).isRequired),
  defaultThemeName: PropTypes.string,
  darkThemeName: PropTypes.string,
  disableLocalStorage: PropTypes.bool,
  scope: PropTypes.oneOf(['global', 'local', false])
};

/**
 * A component that provides the app with theming support.
 * Also providing a `<HydrationChecker/>`.
 */
const ThemeProviderWithHydrationChecker = React.forwardRef<HTMLDivElement, ThemeProviderProps>(
  (props, ref) => {
    return (
      <HydrationChecker>
        <ThemeProvider {...props} ref={ref} />
      </HydrationChecker>
    );
  }
);

ThemeProviderWithHydrationChecker.displayName = 'ThemeProvider';

export default ThemeProviderWithHydrationChecker;
