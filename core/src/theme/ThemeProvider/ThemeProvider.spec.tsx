import React from 'react';
import { mount } from 'enzyme';
import ThemeProvider from './ThemeProvider';
import { dark, light } from '..';
import { createTheme } from '@material-ui/core';
import useTheme, { ThemeUpdate } from '../useTheme';
import 'jest-extended';
import { act } from 'react-dom/test-utils';

const testTheme = createTheme({
  typography: {
    fontFamily: 'TestFontFamily'
  }
});

describe('ThemeProvider', () => {
  it('renders children', () => {
    const wrapper = mount(
      <ThemeProvider>
        <div className="unique" />
      </ThemeProvider>
    );

    expect(wrapper.contains(<div className="unique" />)).toBeTruthy();
  });

  it('applies a font-family to the body, when scope=global', () => {
    mount(<ThemeProvider scope="global" />);

    expect(window.getComputedStyle(document.body).getPropertyValue('font-family')).toEqual(
      light.typography.fontFamily
    );
  });

  it('applies a font-family to a wrapper div, when scope=local', () => {
    const wrapper = mount(<ThemeProvider scope="local" />);

    expect(window.getComputedStyle(wrapper.getDOMNode()).getPropertyValue('font-family')).toEqual(
      light.typography.fontFamily
    );
  });

  it('accepts and applies custom themes', () => {
    mount(<ThemeProvider defaultThemeName="test" themes={{ test: testTheme, Dark: dark }} />);

    expect(window.getComputedStyle(document.body).getPropertyValue('font-family')).toEqual(
      testTheme.typography.fontFamily
    );
  });

  it('picks dark theme if user prefers dark mode', () => {
    jest.spyOn(window, 'matchMedia').mockImplementationOnce((query) => ({
      matches: true,
      media: query,
      onchange: null,
      addListener: jest.fn(), // deprecated
      removeListener: jest.fn(), // deprecated
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn()
    }));

    mount(<ThemeProvider themes={{ Light: light, Dark: testTheme }} />);

    expect(global.matchMedia).toBeCalledWith('(prefers-color-scheme: dark)');
    expect(window.getComputedStyle(document.body).getPropertyValue('font-family')).toEqual(
      testTheme.typography.fontFamily
    );
  });

  it('loads the selected theme from localStorage', () => {
    const getItem = jest
      .spyOn(Object.getPrototypeOf(global.localStorage), 'getItem')
      .mockReturnValue('TestTheme');

    mount(<ThemeProvider themes={{ Light: light, Dark: dark, TestTheme: testTheme }} />);
    expect(getItem).toBeCalledWith('theme');
    expect(window.getComputedStyle(document.body).getPropertyValue('font-family')).toEqual(
      testTheme.typography.fontFamily
    );
  });

  it('skips localStorage if disableLocalStorage is set', () => {
    const getItem = jest.spyOn(Object.getPrototypeOf(global.localStorage), 'getItem');

    mount(<ThemeProvider disableLocalStorage />);
    expect(getItem).not.toBeCalled();
    expect(window.getComputedStyle(document.body).getPropertyValue('font-family')).toEqual(
      light.typography.fontFamily
    );
  });

  it('always loads the default theme during hydration (it must match SSR)', () => {
    const getItem = jest
      .spyOn(Object.getPrototypeOf(global.localStorage), 'getItem')
      .mockReturnValue('TestTheme');

    const renderedThemes: ThemeUpdate[] = [];

    const Child = () => {
      renderedThemes.push(useTheme());
      return null;
    };

    mount(
      <ThemeProvider themes={{ Light: light, Dark: dark, TestTheme: testTheme }}>
        <Child />
      </ThemeProvider>
    );
    expect(getItem).toBeCalledWith('theme');

    // Expect the hydration render to be with SSR defaults
    expect(renderedThemes.shift()).toEqual({
      requestedThemeName: undefined,
      renderedThemeName: 'Light',
      theme: light,
      setTheme: expect.any(Function)
    });
    expect(renderedThemes.shift()).toEqual({
      requestedThemeName: 'TestTheme',
      renderedThemeName: 'TestTheme',
      theme: testTheme,
      setTheme: expect.any(Function)
    });
    expect(renderedThemes).toBeArrayOfSize(0);
  });

  it('loads the light theme if the requested theme does not exist', () => {
    const getItem = jest.spyOn(Object.getPrototypeOf(global.localStorage), 'getItem').mockReturnValue(null);

    const renderedThemes: ThemeUpdate[] = [];

    const Child = () => {
      renderedThemes.push(useTheme());
      return null;
    };

    mount(
      <ThemeProvider defaultThemeName="Does not exist">
        <Child />
      </ThemeProvider>
    );
    expect(getItem).toBeCalledWith('theme');

    // Expect the hydration render to be with SSR defaults
    expect(renderedThemes.shift()).toEqual({
      requestedThemeName: undefined,
      renderedThemeName: 'Does not exist',
      theme: light,
      setTheme: expect.any(Function)
    });

    const renderedTheme = renderedThemes.shift() as ThemeUpdate;
    expect(renderedTheme).toEqual({
      requestedThemeName: undefined,
      renderedThemeName: 'Does not exist',
      theme: light,
      setTheme: expect.any(Function)
    });

    expect(renderedThemes).toBeArrayOfSize(0);

    const consoleAssert = jest.spyOn(console, 'assert').mockImplementationOnce(() => {
      // do nothing.
    });

    getItem.mockReturnValueOnce('Some other non-existing theme');
    act(() => {
      renderedTheme.setTheme('Some other non-existing theme');
    });

    // Expect an error about the theme not existing
    expect(consoleAssert).toBeCalledWith(false, expect.any(String));

    expect(renderedThemes.shift()).toEqual({
      requestedThemeName: 'Some other non-existing theme',
      renderedThemeName: 'Does not exist',
      theme: light,
      setTheme: expect.any(Function)
    });
  });
});
