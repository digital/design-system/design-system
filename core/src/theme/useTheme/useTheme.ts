import React, { useContext } from 'react';
import { light, Theme } from '../themes';

export interface ThemeUpdate {
  /**
   * The name of the theme that the user selected
   */
  requestedThemeName: string | undefined;
  /**
   * The name of the currently rendered theme. This may not match the user selection.
   * they might have selected "Browser Preference" which would either result in "Light" or "Dark"
   */
  renderedThemeName: string;
  /**
   * The current theme
   */
  theme: Theme;
  /**
   * Set the current theme (by name)
   */
  setTheme: (themeName: string | undefined) => void;
}

/** @internal */
export const ThemeUpdateContext = React.createContext<ThemeUpdate>({
  requestedThemeName: undefined,
  renderedThemeName: 'light',
  theme: light,
  setTheme: () => {
    throw Error('Must be used inside a ThemeProvider');
  }
});

/**
 * A hook to get the current theme.
 * Must be used inside a `<ThemeProvider />`
 */
export const useTheme: () => ThemeUpdate = () => useContext(ThemeUpdateContext);
