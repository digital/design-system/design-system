import React, { useEffect, useState } from 'react';
import { mount } from 'enzyme';
import ThemeProvider from '../ThemeProvider';
import { dark, light } from '..';
import { createTheme } from '@material-ui/core';
import useTheme, { ThemeUpdate } from '../useTheme';
import 'jest-extended';

const testTheme = createTheme({
  typography: {
    fontFamily: 'TestFontFamily'
  }
});

describe('useTheme', () => {
  it('allows the theme to be updated', () => {
    let renderedTheme: ThemeUpdate | undefined = undefined;

    const Child = () => {
      const [isHydration, setIsHydration] = useState(true);
      renderedTheme = useTheme();

      const { setTheme } = renderedTheme;

      useEffect(() => {
        if (!isHydration) {
          setTheme('Test');
        }
        setIsHydration(false);
      });

      return null;
    };

    mount(
      <ThemeProvider themes={{ Light: light, Dark: dark, Test: testTheme }}>
        <Child />
      </ThemeProvider>
    );

    expect(window.getComputedStyle(document.body).getPropertyValue('font-family')).toEqual(
      testTheme.typography.fontFamily
    );

    expect(renderedTheme).toEqual({
      requestedThemeName: 'Test',
      renderedThemeName: 'Test',
      theme: testTheme,
      setTheme: expect.any(Function)
    });
  });
});
