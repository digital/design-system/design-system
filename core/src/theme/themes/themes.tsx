import React from 'react';
import { ThemeOptions, Theme as MuiTheme, createTheme } from '@material-ui/core';
import { responsiveFontSizes } from '@material-ui/core';
import { deepmerge } from '@material-ui/utils';

const defaultThemeOptions: Partial<ThemeOptions> = {
  typography: {
    fontFamily: [
      '"Open Sans"',
      // Fallback to system font
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(','),
    h1: {
      fontSize: '4.25rem', // 64px
      lineHeight: 1.125, // 72px
      fontWeight: 900 // Black
    },
    h2: {
      fontSize: '3.125rem', // 50px
      lineHeight: 1.44, // 72px
      fontWeight: 900 // Black
    },
    h3: {
      fontSize: '2.375rem', // 38px
      lineHeight: 1.4736, // 56px
      fontWeight: 700 // Bold
    },
    h4: {
      fontSize: '1.75rem', // 28px
      lineHeight: 1.4285, // 40px
      fontWeight: 700 // Bold
    },
    h5: {
      fontSize: '1.3125rem', // 21px
      lineHeight: 1.714, // 36px
      fontWeight: 700 // Bold
    },
    h6: {
      fontSize: '1.125rem', // 18px
      lineHeight: 1.333, // 24px
      fontWeight: 700 // Bold
    }
  },
  components: {
    MuiRadio: {
      defaultProps: {
        disableTouchRipple: true,
        disableFocusRipple: true,
        icon: (
          <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
            <circle cx="12" cy="12" r="11.5" stroke="#767676" />
          </svg>
        ),
        checkedIcon: (
          <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
            <circle cx="12" cy="12" r="11.5" stroke="#767676" />
            <circle cx="12" cy="12" r="8" fill="#767676" />
          </svg>
        )
      },
      styleOverrides: {
        root: {
          color: '#767676',
          '&.Mui-checked': {
            color: '#767676'
          },
          '&.Mui-focusVisible': {
            backgroundColor: '#b2e5e4'
          }
        }
      }
    }
  }
};

export type Theme = MuiTheme;

export const light: Theme = responsiveFontSizes(
  createTheme(
    deepmerge(defaultThemeOptions, {
      palette: {
        primary: {
          main: '#6BBBAE'
        },
        secondary: {
          main: '#272F36'
        },
        background: {
          default: '#FDFDFD'
        }
      }
    })
  )
);

export const dark: Theme = responsiveFontSizes(
  createTheme(
    deepmerge(defaultThemeOptions, {
      palette: {
        mode: 'dark',
        primary: {
          main: '#6BBBAE'
        },
        secondary: {
          main: '#E6F6F3'
        },
        background: {
          default: '#303030'
        }
      }
    })
  )
);
