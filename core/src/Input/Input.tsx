/* eslint-disable react/prop-types */
import React, { FC } from 'react';
import InputBase, { InputBaseProps } from '@material-ui/core/InputBase';
import { experimentalStyled as styled } from '@material-ui/core';
import { alpha } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

// We don't currently support a secondary colour palette, so we remove this option for now
export type InputProps = Omit<InputBaseProps, 'color'>;

export interface CustomInputProps extends InputProps {
  /**
   * Used to specify a width of the input based on the expected number of characters
   */
  characterWidth?: number;
}

const InputRoot = styled(InputBase, {
  shouldForwardProp: (prop) => prop !== 'characterWidth'
})<CustomInputProps>(({ theme, characterWidth }) => {
  const borderColor = '#878B8F';
  return {
    '& input': {
      lineHeight: 1.5,
      height: '1.5em',
      padding: 0,
      ...(characterWidth ? { width: `${characterWidth + 1}ch` } : {})
    },
    border: `1px solid ${borderColor}`,
    borderRadius: theme.shape.borderRadius,
    padding: '11px 7px',
    '&:hover': {
      borderColor: theme.palette.text.primary
    },
    // Reset on touch devices, it doesn't add specificity
    '@media (hover: none)': {
      '&:hover': {
        borderColor
      }
    },
    '&.Mui-focused': {
      borderColor: theme.palette.primary.main,
      boxShadow: `0 0 0 0.2rem ${alpha(theme.palette.primary.main, 0.2)}`
    },
    '&.Mui-error': {
      borderColor: theme.palette.error.main,
      '&.Mui-focused': {
        boxShadow: `0 0 0 0.2rem ${alpha(theme.palette.error.main, 0.2)}`
      }
    },
    '&.Mui-disabled': {
      borderColor: theme.palette.action.disabled
    }
  };
});

const Input: FC<CustomInputProps> = React.forwardRef(function Input(props, ref) {
  return <InputRoot {...props} ref={ref} />;
});

Input.propTypes = {
  ...(InputBase as FC<CustomInputProps>).propTypes,
  characterWidth: PropTypes.number
};

export default Input;
