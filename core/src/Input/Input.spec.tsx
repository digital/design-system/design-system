import * as React from 'react';
import Input from './Input';
import { shallow } from 'enzyme';

describe('Input', () => {
  it('renders component', () => {
    expect(shallow(<Input />)).toMatchSnapshot();
  });
});
