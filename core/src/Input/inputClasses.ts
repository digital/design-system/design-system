import { generateUtilityClasses, generateUtilityClass } from '@material-ui/unstyled';

export function getInputUtilityClasses(slot: string): string {
  return generateUtilityClass('UcamInput', slot);
}

export const inputClasses = generateUtilityClasses('UcamInput', [
  'root',
  'formControl',
  'focused',
  'disabled',
  'adornedStart',
  'adornedEnd',
  'error',
  'sizeSmall',
  'multiline',
  'colorSecondary',
  'fullWidth',
  'hiddenLabel',
  'input',
  'inputSizeSmall',
  'inputMultiline',
  'inputTypeSearch',
  'inputAdornedStart',
  'inputAdornedEnd',
  'inputHiddenLabel'
]);
