import React, { FC } from 'react';
import { Typography, TypographyProps } from '@material-ui/core';

// eslint-disable-next-line react/prop-types
export const P: FC<TypographyProps> = ({ variant = 'body1', ...props }) => {
  return <Typography {...props} variant={variant} paragraph />;
};
// eslint-disable-next-line react/prop-types
export const H1: FC<TypographyProps> = ({ variant = 'h1', ...props }) => {
  return <Typography {...props} variant={variant} gutterBottom />;
};
// eslint-disable-next-line react/prop-types
export const H2: FC<TypographyProps> = ({ variant = 'h2', ...props }) => {
  return <Typography {...props} variant={variant} gutterBottom />;
};
// eslint-disable-next-line react/prop-types
export const H3: FC<TypographyProps> = ({ variant = 'h3', ...props }) => {
  return <Typography {...props} variant={variant} gutterBottom />;
};
// eslint-disable-next-line react/prop-types
export const H4: FC<TypographyProps> = ({ variant = 'h4', ...props }) => {
  return <Typography {...props} variant={variant} gutterBottom />;
};
// eslint-disable-next-line react/prop-types
export const H5: FC<TypographyProps> = ({ variant = 'h5', ...props }) => {
  return <Typography {...props} variant={variant} gutterBottom />;
};
// eslint-disable-next-line react/prop-types
export const H6: FC<TypographyProps> = ({ variant = 'h6', ...props }) => {
  return <Typography {...props} variant={variant} gutterBottom />;
};
