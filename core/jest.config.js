/* eslint-env node */

module.exports = {
  roots: ['<rootDir>/src'],
  testMatch: ['**/__tests__/**/*.+(ts|tsx|js)', '**/?(*.)+(spec|test).+(ts|tsx|js)'],
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest'
  },
  setupFilesAfterEnv: ['jest-extended', `<rootDir>/testSetup.js`],
  snapshotSerializers: [`enzyme-to-json/serializer`],
  restoreMocks: true,
  clearMocks: true,
  resetMocks: true,
  moduleNameMapper: {
    // Material-ui is esm by default. Jest runs with commonjs, so we point it at the commonjs folder (/node)
    '^@material-ui/core$': '<rootDir>/node_modules/@material-ui/core/node',
    '^@material-ui/core/(.*)$': '<rootDir>/node_modules/@material-ui/core/node/$1'
  },
  reporters: ['jest-standard-reporter']
};
