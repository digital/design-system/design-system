declare module '@material-ui/core/FormControl/formControlState' {
  // Exported by Material-ui but not currently part of the type definition
  export default function formControlState({ props, states, muiFormControl }): any; // eslint-disable-line @typescript-eslint/no-explicit-any
}
