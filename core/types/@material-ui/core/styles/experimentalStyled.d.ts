declare module '@material-ui/core/styles/experimentalStyled' {
  interface RootShouldForwardProp {
    <Props, ForwardedProp extends keyof Props = keyof Props>(
      propName: PropertyKey
    ): propName is ForwardedProp;
  }

  // Exported by Material-ui but not currently part of the type definition
  export const rootShouldForwardProp: RootShouldForwardProp;
}

// Tell typescript that this a module
export {};
