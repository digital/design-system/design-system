declare module '@material-ui/core/InputBase/InputBase' {
  // Exported by Material-ui but not currently part of the type definition
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  export const overridesResolver: (props: unknown, styles: unknown) => any;
}
