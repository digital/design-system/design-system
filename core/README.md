# University of Cambridge - Design System

Welcome to the University of Cambridge - Design System.

This project is in active development and does not yet have any stable releases.

# Installation

```shell
npm install @ucam/design-system
```

# Peer Dependencies

```shell
npm install react react-dom @material-ui/core
```

# Usage

```typescript jsx
import Button from "@ucam/design-system/Button";

const App = () => <Button>Hello World</Button>;
```

# Documentation

Coming Soon!
