/* eslint-env node */
import docgen from 'react-docgen-typescript';
import fs from 'fs';
import path from 'path';

// The delimiter for both the start and end of the property table's position in the component's
// `*.doc.mdx` file.
const TABLE_DELIMITER = "[//]: # 'property-table-DO-NOT-EDIT'\n";

// The title preceeding the property table
const TABLE_TITLE = '### Component Properties\n\n';

// The property table's header
const TABLE_HEADER = '| Prop name | Type | Default | Description |\n| -- | -- | -- | -- |\n';

/**
 * Generator that recursively yields all files in a directory
 */
async function* walk(dir) {
  for await (const d of await fs.promises.opendir(dir)) {
    const entry = path.join(dir, d.name);
    if (d.isDirectory()) {
      yield* await walk(entry);
    } else if (d.isFile()) {
      yield entry;
    }
  }
}

/**
 * Parse a component's source file for it's properties, filter and sort, and then render as a
 * markdown table (with title).
 */
function generateTable(sourceFile) {
  // Uses the "react-docgen-typescript" library to parse the component's source file for it's
  // properties.
  const result = docgen.parse(sourceFile, { savePropValueAsString: true });

  const properties = Object.values(result[0].props)
    // not interested in documenting "aria-*" properties
    .filter(({ name }) => !name.startsWith('aria-'))
    // only display properties that have a description
    .filter(({ description }) => description);

  // Don't bother returning a table if there are no properties to document.
  if (properties.length === 0) {
    return '';
  }

  return (
    properties
      // sort by `required` then name
      .sort(({ name: name1, required: required1 }, { name: name2, required: required2 }) => {
        if (required1 === required2) {
          return name1.localeCompare(name2);
        } else {
          return required1 ? -1 : 1;
        }
      })
      // format a property's attributes into a row of the table
      .reduce((body, { name, type, required, defaultValue, description }) => {
        // escape '|' and '>'
        const cleanedType = type.name.replace(/\|/g, '\\|').replace(/>/g, '\\>');
        // remove newlines
        const cleanedDescription = description.replace(/\n/g, ' ');
        // combine `required` and `defaultValue` (removing newlines)
        const cleanedDefaultValue = (() => {
          if (required) {
            return 'Required';
          } else if (defaultValue) {
            return defaultValue.value.replace(/\n/g, ' ');
          } else {
            return '';
          }
        })();
        return `${body}| ${name} | ${cleanedType} | ${cleanedDefaultValue} | ${cleanedDescription} |\n`;
      }, TABLE_TITLE + TABLE_HEADER)
  );
}

/**
 * For a given component source file, attempts to update the component's markdown file (if it has
 * one) with an updated property table.
 */
async function generateDocumentProperties(sourceFile) {
  console.log(`parsing ${sourceFile}`);

  const pathParts = path.parse(sourceFile);

  const markdownFile = path.join(pathParts.dir, pathParts.name + '.doc.mdx');

  try {
    // try to open the component's markdown file
    const document = await fs.promises.readFile(markdownFile, {
      encoding: 'utf-8'
    });
    const parts = document.split(TABLE_DELIMITER);
    if (parts.length !== 3) {
      // we expect the 2 table delimiters to already be in the file
      console.warn(`table delimiters not found in ${markdownFile} .. skipping`);
    } else {
      // re-write the component's markdown file with an updated property table
      await fs.promises.writeFile(
        markdownFile,
        parts[0] + TABLE_DELIMITER + generateTable(sourceFile) + TABLE_DELIMITER + parts[2]
      );
    }
  } catch (err) {
    if (err.errno == -2) {
      console.warn(`${markdownFile} doesn't exist .. skipping`);
    } else {
      throw err;
    }
  }
}

/**
 * Scans a given folder for all component source files and attempts to document their properties.
 */
(async () => {
  // script parameter(s)
  var [srcDir] = process.argv.slice(2);

  // filter out unittest files
  for await (const sourceFile of walk(srcDir))
    if (sourceFile.endsWith('.tsx') && !sourceFile.endsWith('.spec.tsx')) {
      await generateDocumentProperties(sourceFile);
    }
})();
