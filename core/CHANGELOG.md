# Change Log - @ucam/design-system

This log was last generated on Wed, 29 Sep 2021 07:51:03 GMT and should not be manually modified.

## 0.9.0
Wed, 29 Sep 2021 07:51:03 GMT

### Minor changes

- Add storybook

## 0.8.1
Thu, 23 Sep 2021 15:09:59 GMT

### Patches

- Updating the design-system props documentation

## 0.8.0
Wed, 22 Sep 2021 16:37:16 GMT

### Minor changes

- Added prop to TextField to support passing props directly to the FormControl component. Enables features like using FullWidth.

## 0.7.3
Wed, 08 Sep 2021 15:28:47 GMT

### Patches

- Fix theming TS errors

## 0.7.2
Mon, 21 Jun 2021 16:07:53 GMT

### Patches

- Changes experimentalStyled import in other component files

## 0.7.1
Mon, 21 Jun 2021 13:38:21 GMT

### Patches

- Changes experimentalStyled import in Sidebar

## 0.7.0
Thu, 10 Jun 2021 12:00:43 GMT

### Minor changes

- Refactor TextField to properly apply input props and types

## 0.6.1
Tue, 08 Jun 2021 11:08:53 GMT

### Patches

- Fix Sidebar server rendering issue

## 0.6.0
Mon, 07 Jun 2021 13:41:08 GMT

### Minor changes

- Adds ability to set the size of an input box

## 0.5.0
Wed, 26 May 2021 16:00:16 GMT

### Minor changes

- Adds focus styles to button component

## 0.4.0
Tue, 25 May 2021 17:25:24 GMT

### Minor changes

- Add hint and feedback options to TextField component

## 0.3.0
Wed, 19 May 2021 21:03:08 GMT

### Minor changes

- Adds a Radio component restyled from Material-UI

## 0.2.0
Thu, 13 May 2021 11:12:35 GMT

### Minor changes

- Updated the version of material-ui that the design system is built on to the latest alpha

## 0.1.0
Tue, 04 May 2021 08:31:50 GMT

### Minor changes

- Adds a Checkbox component that supports an optional label

### Patches

- Moved repo to RushJs - Starting changelog

